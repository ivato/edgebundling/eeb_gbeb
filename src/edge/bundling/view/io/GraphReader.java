package edge.bundling.view.io;

import edge.bundling.compatibility.Compatibility;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundled;
import edge.bundling.graph.Node;
import edge.bundling.methods.cover.Cover;
import edge.bundling.util.Flags;
import edge.bundling.util.GraphData;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class GraphReader implements InterfaceGraphReader {

    //*************************************************************************************************************
    /// <summary>
    /// Importa um arquivo GRAPHML e gera um grafo simples
    /// </summary>
    /// <param name="file">Arquivo xml dos dados</param>
//*************************************************************************************************************                           
    public Graph importGraph(File file, GraphBundled graphBundled) throws Exception {
        Graph graph = new Graph(false);
        open(file, graph, graphBundled);
        Flags.GRAPH(graph);
        return graph;
    }

    //*************************************************************************************************************
    /// <summary>
    /// Método auxiliar na importação de um arquivo GRAPHML
    /// </summary>
    /// <param name="file">Arquivo xml dos dados</param>
    /// <param name="graph">Grafo gerado a partir do GRAPHML </param>
//*************************************************************************************************************                   
    private void open(File file, Graph graph, GraphBundled graphBundled) {

        graph.clear();
        graphBundled.clear();

        try {
            SAXBuilder builder = new SAXBuilder();

            Document document = (Document) builder.build(file);
            Element root = document.getRootElement();

            //Recupera os dados dos vértices
            String x = "0", y = "0";
            int id = 0;
            String data;
            Node node;
            Iterator<Object> iter_node = document.getDescendants(new org.jdom.filter.ElementFilter("node"));
            while (iter_node.hasNext()) {
                x = "0";
                y = "0";
                Object o = iter_node.next();
                if (o instanceof org.jdom.Element) {
                    org.jdom.Element element = (org.jdom.Element) o;
                    Iterator<Object> iterData = element.getDescendants(new org.jdom.filter.ElementFilter("data"));
                    while (iterData.hasNext()) {
                        Object o2 = iterData.next();
                        if (o2 instanceof org.jdom.Element) {
                            org.jdom.Element dataElement = (org.jdom.Element) o2;
                            if (dataElement.getAttributeValue("key").equals("x")) {
                                x = dataElement.getContent(0).getValue().trim();
                            } else {
                                if (dataElement.getAttributeValue("key").equals("y")) {
                                    y = dataElement.getContent(0).getValue().trim();
                                }
                            }
                        }
                    }
                    data = element.getAttributeValue("id");

                    double temp1 = Double.parseDouble(x);
                    double temp2 = Double.parseDouble(y);

                    node = new Node(id, data, temp1, temp2);
                    graph.addNode(node);
                    graphBundled.addNode(node);

                    id++;
                }
            }

            //Recupera os dados das arestas
            int weight = 1;
            Node nodeStart;
            Node nodeEnd;

            id = 0;
            Iterator<Object> iter_edge = document.getDescendants(new org.jdom.filter.ElementFilter("edge"));
            while (iter_edge.hasNext()) {
                Object o = iter_edge.next();
                if (o instanceof org.jdom.Element) {
                    org.jdom.Element element = (org.jdom.Element) o;
                    String idNode1 = element.getAttribute("source").getValue();
                    String idNode2 = element.getAttribute("target").getValue();

                    data = String.valueOf(id);
                    weight = 1;

                    nodeStart = graph.searchNode(idNode1);
                    nodeEnd = graph.searchNode(idNode2);

                    Edge edge = new Edge(id, data, nodeStart, nodeEnd, weight);

                    graph.addEdge(edge);
                    graphBundled.addEdge(edge);
                    id++;
                }

            }
            
            GraphData.setAdjacency(graph);

            //Encontra os pontos extremos para x e y no grafos
            double maxX = graph.getNodeList().get(0).getX();
            double minX = graph.getNodeList().get(0).getX();
            double maxY = graph.getNodeList().get(0).getY();
            double minY = graph.getNodeList().get(0).getY();
            for (int i = 1; i < graph.getNodeList().size(); i++) {
                if (maxX < graph.getNodeList().get(i).getX()) {
                    maxX = graph.getNodeList().get(i).getX();
                }
                if (maxY < graph.getNodeList().get(i).getY()) {
                    maxY = graph.getNodeList().get(i).getY();
                }
                if (minX > graph.getNodeList().get(i).getX()) {
                    minX = graph.getNodeList().get(i).getX();
                }
                if (minY > graph.getNodeList().get(i).getY()) {
                    minY = graph.getNodeList().get(i).getY();
                }
            }

            //Encontra o meio da tela e o meio do grafo
            double middleScreenY = Flags.XSCREENSIZE() / 2;
            double middleScreenX = Flags.YSCREENSIZE() / 2;
            double middleGraphY = (minY + maxY) / 2;
            double middleGraphX = (minX + maxX) / 2;

            //Encontra a diferença que permitirá centralizar o grafo na tela
            double diffX = middleGraphX - middleScreenX;
            double diffY = middleGraphY - middleScreenY;

            double x1, y1;
            for (int i = 0; i < graph.getNodeList().size(); i++) {

                //Calcula a posição dos vértice que permite a centralização
                x1 = graph.getNodeList().get(i).getX() - diffX;
                y1 = graph.getNodeList().get(i).getY() - diffY;

                graph.getNodeList().get(i).setX(x1);
                graph.getNodeList().get(i).setY(y1);
                graph.getNodeList().get(i).setxOriginal(x1);
                graph.getNodeList().get(i).setyOriginal(y1);

            }

            graph.area();
            graphBundled.setArea(graph.getArea());
            graph.resetScale();

            //Ajusta a posição dos vértices para que caibam na tela conforme resolução inicial
            ajustBoundPosition(graph, graphBundled);
            
            try{
                //Calcula a cobertura
                Cover cover = new Cover();
                cover.vertexCoverSet(graph);
                graphBundled.setCoverList(cover.getCoverList());
    
            }catch(Exception e){
            
            }

        } catch (IOException ie) {
            System.out.println(ie.getMessage());
        } catch (JDOMException e) {
            System.out.println(e.getMessage());
        }
    }

    //*************************************************************************************************************
    /// Procedimento que deixa a posição do nó limitados a janela
//*************************************************************************************************************
    private void ajustBoundPosition(Graph graph, GraphBundled graphBundled) {
        //Encontra os pontos extremos para x e y no grafos
        double maxX = graph.getNodeList().get(0).getX();
        double minX = graph.getNodeList().get(0).getX();
        double maxY = graph.getNodeList().get(0).getY();
        double minY = graph.getNodeList().get(0).getY();

        for (int i = 1; i < graph.getNodeList().size(); i++) {
            if (maxX < graph.getNodeList().get(i).getX()) {
                maxX = graph.getNodeList().get(i).getX();
            }
            if (maxY < graph.getNodeList().get(i).getY()) {
                maxY = graph.getNodeList().get(i).getY();
            }
            if (minX > graph.getNodeList().get(i).getX()) {
                minX = graph.getNodeList().get(i).getX();
            }
            if (minY > graph.getNodeList().get(i).getY()) {
                minY = graph.getNodeList().get(i).getY();
            }
        }

        if ((minY < 0) || (minX < 0) || (maxY > Flags.YSCREENSIZE()) || (maxX > Flags.XSCREENSIZE())) {
            double x, y;

            for (int i = 0; i < graph.getNodeList().size(); i++) {
                Node n = graph.getNodeList().get(i);
                Node n2 = graphBundled.getNodeList().get(i);

                x = (n.getX() * Flags.SCALE()) / (Flags.SCALE() * 2);
                y = (n.getY() * Flags.SCALE()) / (Flags.SCALE() * 2);

                n.setX(x);
                n.setY(y);
                n.setxOriginal(x);
                n.setyOriginal(y);

                n2.setX(x);
                n2.setY(y);
                n2.setxOriginal(x);
                n2.setyOriginal(y);

            }

            centralizePosition(graph, graphBundled);

            graph.area();
            graphBundled.setArea(graph.getArea());
            
        }
    }
  
    //*************************************************************************************************************
    /// Procedimento que centraliza o grafo na janela
//*************************************************************************************************************
    private void centralizePosition(Graph graph, GraphBundled graphBundled) {
        double maxX = graph.getNodeList().get(0).getX();
        double minX = graph.getNodeList().get(0).getX();
        double maxY = graph.getNodeList().get(0).getY();
        double minY = graph.getNodeList().get(0).getY();

        for (int i = 1; i < graph.getNodeList().size(); i++) {
            if (maxX < graph.getNodeList().get(i).getX()) {
                maxX = graph.getNodeList().get(i).getX();
            }

            if (maxY < graph.getNodeList().get(i).getY()) {
                maxY = graph.getNodeList().get(i).getY();
            }

            if (minX > graph.getNodeList().get(i).getX()) {
                minX = graph.getNodeList().get(i).getX();
            }

            if (minY > graph.getNodeList().get(i).getY()) {
                minY = graph.getNodeList().get(i).getY();
            }
        }
        double middleScreenY = Flags.XSCREENSIZE() / 2;
        double middleScreenX = Flags.YSCREENSIZE() / 2;

        double middleGraphY = (minY + maxY) / 2;
        double middleGraphX = (minX + maxX) / 2;

        double diffX = middleGraphX - middleScreenX;
        double diffY = middleGraphY - middleScreenY;

        double x1, y1;

        for (int i = 0; i < graph.getNodeList().size(); i++) {
            x1 = graph.getNodeList().get(i).getX();
            graph.getNodeList().get(i).setX(x1 - diffX);
            graph.getNodeList().get(i).setxOriginal(x1 - diffX);

            graphBundled.getNodeList().get(i).setX(x1 - diffX);
            graphBundled.getNodeList().get(i).setxOriginal(x1 - diffX);

            y1 = graph.getNodeList().get(i).getY();
            graph.getNodeList().get(i).setY(y1 - diffY);
            graph.getNodeList().get(i).setyOriginal(y1 - diffY);

            graphBundled.getNodeList().get(i).setY(y1 - diffY);
            graphBundled.getNodeList().get(i).setyOriginal(y1 - diffY);
        }
    }
}
