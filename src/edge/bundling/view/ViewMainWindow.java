package edge.bundling.view;

import edge.bundling.controller.ControllerMainWindow;
import edge.bundling.view.rendering.LinearDrawPanelGraph;
import edge.bundling.view.rendering.LinearDrawPanelGraphBundled;
import edge.bundling.view.components.ComponentParametersPanel;
import edge.bundling.view.components.ComponentToolBar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;

public class ViewMainWindow extends JPanel {

    private static final long serialVersionUID = 1L;

    private LinearDrawPanelGraph drawPanelGraph;
    private LinearDrawPanelGraphBundled drawPanelGraphBundled;
    private ComponentParametersPanel paramPanel;
    private ComponentToolBar toolBarPanel;
    private JScrollPane drawScroll = null;
    private JScrollPane paramsScroll = null;

    private ControllerMainWindow windowPanelController;

    
    public ViewMainWindow() {

        this.setLayout(new BorderLayout(3, 3));
        this.setBorder(new EmptyBorder(5, 5, 5, 5));

        toolBarPanel = new ComponentToolBar();
        paramPanel = new ComponentParametersPanel();
        drawPanelGraph = new LinearDrawPanelGraph();
        drawPanelGraphBundled = new LinearDrawPanelGraphBundled();

        paramsScroll = new JScrollPane(paramPanel);
        paramsScroll.setPreferredSize(new Dimension((int) paramPanel.getSize().getWidth(), (int) paramPanel.getSize().getHeight()));

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 900, 687);
        panel.setBackground(Color.WHITE);
        drawScroll = new JScrollPane(drawPanelGraph,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        drawScroll.setPreferredSize(new Dimension((int) panel.getSize().getWidth(), (int) panel.getSize().getHeight()));
       
    
        windowPanelController = new ControllerMainWindow(this, drawScroll, toolBarPanel, paramPanel, drawPanelGraph, drawPanelGraphBundled);
        windowPanelController.initController();

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, paramsScroll, drawScroll);
        this.add(toolBarPanel, java.awt.BorderLayout.PAGE_START);
        this.add(splitPane, BorderLayout.CENTER);
    }
    


}
