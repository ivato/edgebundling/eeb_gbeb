package edge.bundling.util;

import edge.bundling.graph.Edge;
import edge.bundling.graph.Node;
import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

public class Geometric {

    //*************************************************************************************************************
    /**
     * Calcula o feixo convexo entre uma lista de pontos
     *
     * @param points - pontos de entrada
     * @return lista dos pontos do feixo convexo
     */
//*************************************************************************************************************     
    public static ArrayList<Point.Double> convexHull(ArrayList<Point.Double> points) {
        ArrayList<Point.Double> pointsOfHull = new ArrayList<Point.Double>();

        // Se existir apenas três pontos eles serão o feixo convexo
        if (points.size() < 3) {
            return points;
        }

        List<Point.Double> hull = new ArrayList<Point.Double>();

        // Encontra o ponto mais a esquerda
        int l = 0;
        for (int i = 1; i < points.size(); i++) {
            if (points.get(i).x < points.get(l).x) {
                l = i;
            }
        }

        //Iniciando com o ponto mais a esquerda move-se no setindo antiorário até 
        //alcançar o ponto inicial
        int p = l, q;
        do {
            // Adiciona o ponto corrente ao feixo
            hull.add(points.get(p));

            //Busca pelo ponto q cuja orientação (p, x, q) é horária para todos os pontos x
            q = (p + 1) % points.size();
            for (int i = 0; i < points.size(); i++) {
                //Se e é mais antiorário que p, atualiza q
                if (orientation(points.get(p), points.get(i), points.get(q)) == 2) {
                    q = i;
                }
            }

            //q é o mais antiorário com base em p. Seta p como q para a próxima iteração
            p = q;
            //System.out.println("p:" + p + " l: " + l);
        } while (p != l);

        ListIterator<Point.Double> listIterator = hull.listIterator();
        while (listIterator.hasNext()) {
            pointsOfHull.add(listIterator.next());
        }

        return pointsOfHull;
    }

    //*************************************************************************************************************
    /**
     * Encontra a orientação da tripla (p, q, r)
     *
     * @param p
     * @param q
     * @param r
     * @return orientação: 0 se forem colineares, 1 se for horário e 2 se
     * antiorário
     */
//*************************************************************************************************************     
    private static int orientation(Point.Double p, Point.Double q, Point.Double r) {
        int val = (int) ((q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y));
        if (val == 0) {
            return 0;
        }
        return (val > 0) ? 1 : 2;
    }

    //*************************************************************************************************************
    /**
     * Calcula a distância euclidiana entre dois pontos (tamanho da aresta)
     *
     * @param P - ponto 1
     * @param Q - ponto 2
     * @return distance
     */
//*************************************************************************************************************     
    public static double euclideanDistance(Point.Double p, Point.Double q) {
        return Math.sqrt(Math.pow(p.x - q.x, 2) + Math.pow(p.y - q.y, 2));
    }

    //*************************************************************************************************************
    /**
     * Calcula a visibilidade entre duas arestas
     *
     * @param P1 - ponto 1 da aresta 1
     * @param P2 - ponto 2 da aresta 1
     * @param P3 - ponto 1 da aresta 2
     * @param P4 - ponto 2 da aresta 2
     * @return visibility
     */
//*************************************************************************************************************     
    public static double edgeVisibility(Point.Double P1, Point.Double P2, Point.Double P3, Point.Double P4) {
        Point.Double I0 = projectPointOnLine(P3, P1, P2);
        Point.Double I1 = projectPointOnLine(P4, P1, P2);

        double x = (I0.x + I1.x) / 2.0;
        double y = (I0.y + I1.y) / 2.0;
        Point.Double midI = new Point.Double(x, y);

        x = (P1.x + P2.x) / 2.0;
        y = (P1.y + P2.y) / 2.0;
        Point.Double midP = new Point.Double(x, y);

        double result = Math.max(0, (1 - ((2 * euclideanDistance(midP, midI)) / euclideanDistance(I0, I1))));

        return result;
    }

    //*************************************************************************************************************
    /**
     * Calcula a projeção de um ponto em uma linha
     *
     * @param p - ponto a ser projetado
     * @param P - ponto 1 da linha
     * @param Q - ponto 2 da linha
     * @return proint
     */
//*************************************************************************************************************     
    public static Point.Double projectPointOnLine(Point.Double p, Point.Double P, Point.Double Q) {
        double L = Math.sqrt((Q.x - P.x) * (Q.x - P.x) + (Q.y - P.y) * (Q.y - P.y));
        double r = ((P.y - p.y) * (P.y - Q.y) - (P.x - p.x) * (Q.x - P.x)) / (L * L);

        double x = P.x + r * (Q.x - P.x);
        double y = P.y + r * (Q.y - P.y);
        Point.Double point = new Point.Double(x, y);
        return point;
    }
  
    public static Point.Double newVector(Point p1, Point p2){
        Point.Double new1 = new Point.Double((p2.x - p1.x), (p2.y - p1.y));
        return new1;
    }   
    public static Point.Double unitVector(Point.Double a){
        double x = a.x/(Math.sqrt(a.x*a.x + a.y*a.y));
        double y = a.y/(Math.sqrt(a.x*a.x + a.y*a.y));
        Point.Double new1 = new Point.Double(x,y);  
        return new1;
    }   
    
    public static double  vector_dot_product(Point.Double p, Point.Double q) {
        return p.x * q.x + p.y * q.y;
    }
    
   public static double edge_length(Edge e) {
        // handling nodes that are on the same location, so that K/edge_length != Inf
        if (Math.abs(e.getStartNode().getX() - e.getEndNode().getX()) < 1e-6 && Math.abs(e.getStartNode().getY() - e.getEndNode().getY()) < 1e-6)
            return 1e-6;
        return Math.sqrt(Math.pow(e.getEndNode().getX() - e.getEndNode().getX(), 2) + Math.pow(e.getEndNode().getY() - e.getEndNode().getY(), 2));
    }
    
   
    public static double dx(Point.Double p1, Point.Double p2) {
        return p2.getX() - p1.getX();
    }

    public static double dy(Point.Double p1, Point.Double p2){
        return p2.getY() - p1.getY();
    }

    public static double magnitude(Point.Double p1, Point.Double p2) {
        return Math.sqrt(dx(p1,p2) * dx(p1,p2) + dy(p1,p2) * dy(p1,p2));
    }
   
      
    public static Node midPoint(Point.Double p1, Point.Double p2) {
        double x = (p1.getX() + p2.getX()) / 2;
        double y = (p1.getY() + p2.getY()) / 2;

        Node node = new Node();
        node.setX(x);
        node.setY(y);

        return node;
    }
    
   public static Node lineIntersection(Point.Double p1, Point.Double p2, Point.Double p3, Point.Double p4) {
        double x1 = p1.getX();
        double y1 = p1.getY();
        double x2 = p2.getX();
        double y2 = p2.getY();
        double x3 = p3.getX();
        double y3 = p3.getY();
        double x4 = p4.getX();
        double y4 = p4.getY();

        if ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4) == 0) {
            return null;
        }
        double px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
        double py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));

        return new Node(px, py);
    }
    
    public static double angleBetween2Lines(Point.Double p1, Point.Double p2, Point.Double p3, Point.Double p4){
        
        /*double a = dx(p1,p2);
        double b = dx(p3,p4);
        double c = dy(p1,p2);
        double d = dy(p3,p4);
        double e = magnitude(p1,p2);
        double f = magnitude(p3,p4);
        double g = (a * b + c * d);
        double h = (e * f);
        double i = g/h;
        double j = Math.abs(i);*/
        
        double angle = Math.abs((dx(p1,p2) * dx(p3,p4) + dy(p1,p2) * dy(p3,p4)) / (magnitude(p1,p2) * magnitude(p3,p4)));
        if (angle <= 1 ){
            angle = Math.acos(angle); 
        }else{
            angle = -1;
        }
        
        //double angle = Math.acos(Math.abs((dx(p1,p2) * dx(p3,p4) + dy(p1,p2) * dy(p3,p4)) / (magnitude(p1,p2) * magnitude(p3,p4))));         
        
        return angle;
    }
    
    
    //*************************************************************************************************************
    /**
     * Calcula o angulo entre duas retas
     *
     * @param P1 - ponto 1 da reta 1
     * @param P2 - ponto 2 da reta 1
     * @param P3 - ponto 1 da reta 2
     * @param P4 - ponto 2 da reta 2
     * @return angle
     */
//*************************************************************************************************************     
    public static double angle(Point.Double p1, Point.Double p2, Point.Double p3, Point.Double p4) {
        double angle1 = 0;
        double angle2 = 0;
        double angle = 0;
        double valueAngle = 0;
        BigDecimal value = null;

        //Fórmula dos artigos
        //double num = vectorDotProduct(edgeAsVector(p2, p), edgeAsVector(p3, p));
        //double den = (edgeLength(p2, p) * edgeLength(p3, p));
        //double result = Math.abs(num / den);
       
        //Minha fórmula
        angle1 = Math.atan2(p1.y - p2.y, p1.x - p2.x);
        angle2 = Math.atan2(p3.y - p4.y, p3.x - p4.x);
        if (Math.toDegrees(angle1 - angle2) > 180) {
            valueAngle = Math.abs(360 - Math.toDegrees(angle1 - angle2));
        } else {
            valueAngle = Math.abs(Math.toDegrees(angle1 - angle2));
        }
        value = new BigDecimal(valueAngle).setScale(0, RoundingMode.HALF_DOWN);
        angle = (double) value.doubleValue();

        //System.out.println("Angulo Bruto: " + angle);
        
        if (angle > 180) {
            angle = 360 - angle;
        }

        return (double) angle;
    }

    //*************************************************************************************************************
    /**
     * Normaliza um valor de angulo
     *
     * @param angle - angulo a ser normalizado
     * @return value
     */
//*************************************************************************************************************     
    public static double angleNormalized(double angle) {
        double v = 0;
        BigDecimal value = null;
        v = 1 - (angle * 0.5) / 90;
        value = new BigDecimal(v).setScale(3, RoundingMode.HALF_DOWN);
        return (double) value.doubleValue();
    }
    
    
    public static Point.Double project(Point.Double p, Point.Double p1, Point.Double p2){
        double L = Geometric.euclideanDistance(p1, p2);
        double r = ((p1.y-p.y)*(p1.y-p2.y) - (p1.x-p.x)*(p2.x-p1.x)) / (L * L);
        double x = p1.x + (p2.x-p1.x) * r;
        double y = p1.y + (p2.y-p1.y) * r;
    
        Point.Double point = new Point.Double(x,y);
        return point;
    }

    
   public static Point.Double center(Point.Double p1, Point.Double p2){
        double x = (p1.x + p2.x)/2;
        double y = (p1.y + p2.y)/2;
    
        Point.Double point = new Point.Double(x,y);
        return point;
    }
}
