package edge.bundling.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {

    public File file;
    public BufferedWriter bw;
    Date dateInitial;

    public void create(String name) {
        try {
            file = new File(name);
            bw = new BufferedWriter(new FileWriter(file));

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void create(String name, boolean flag) {
        try {
            file = new File(name);
            bw = new BufferedWriter(new FileWriter(file, flag));

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void start() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        dateInitial = new Date();
    }

    public void finish(String parameters, String solution) {
        try {
            if (file.canWrite()) {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Date dateFinal = new Date();

                String log = "";

                log += dateFormat.format(dateInitial) + "\t" + dateFormat.format(dateFinal) + "\t";
                bw.write(log + parameters + solution);
                bw.newLine();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void save(String msg) {
        try {
            if (file.canWrite()) {
                bw.write(msg);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void close() {
        try {
            bw.flush();
            bw.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
