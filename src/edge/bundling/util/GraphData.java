package edge.bundling.util;

import edge.bundling.compatibility.Compatibility;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Graph;
import edge.bundling.graph.Node;
import java.awt.Point;
import java.util.ListIterator;

public class GraphData {

    private static int[][] adjacency;
    private static int[][] commomNode;
    private static double[][] angles;
    private static double[][] compatibility;
    private static double[][] compatibilityAngular;
    private static double[][] compatibilityScale;
    private static double[][] compatibilityPosition;
    private static double[][] compatibilityVisibility;
    private static double[][] compatibilityDistance;

    
   private static boolean isAdjacent(Edge edge1, Edge edge2) {
        if (edge1.getStartNode() == edge2.getStartNode()) {
            commomNode[edge1.getId()][edge2.getId()] = edge1.getStartNode().getId();
            return true;
        }
        if (edge1.getStartNode() == edge2.getEndNode()) {
            commomNode[edge1.getId()][edge2.getId()] = edge1.getStartNode().getId();
            return true;
        }
        if (edge1.getEndNode() == edge2.getStartNode()) {
            commomNode[edge1.getId()][edge2.getId()] = edge1.getEndNode().getId();
            return true;
        }
        if (edge1.getEndNode() == edge2.getEndNode()) {
            commomNode[edge1.getId()][edge2.getId()] = edge1.getEndNode().getId();
            return true;
        }
        return false;
    }
    
    public static void setAdjacency(Graph graph){
        
        adjacency = new int[graph.getSizeEdge()][graph.getSizeEdge()];
        commomNode = new int[graph.getSizeEdge()][graph.getSizeEdge()];
        
        for (int i = 0; i < adjacency.length; i++) {
            for (int j = 0; j < adjacency[0].length; j++) {
                adjacency[i][j] = 0;
                commomNode[i][j] = -1;
            }
        }
        
        ListIterator<Edge> listIterator1 = graph.getEdgeList().listIterator();
        while (listIterator1.hasNext()) {
            Edge edge1 = listIterator1.next();        
            ListIterator<Edge> listIterator2 = graph.getEdgeList().listIterator();
            while (listIterator2.hasNext()) {
                Edge edge2 = listIterator2.next();               
                if (isAdjacent(edge1, edge2)){
                    adjacency[edge1.getId()][edge2.getId()] = 1;
                    adjacency[edge2.getId()][edge1.getId()] = 1;
                }
            }   
        } 
        
        /*for (int i = 0; i < adjacency.length; i++) {
            for (int j = 0; j < adjacency[0].length; j++) {
                System.out.print("[" + adjacency[i][j] + "]\t");
            }
            System.out.println();
        }*/
    }
    
    //*************************************************************************************************************
    /**
     * Calcula a matriz de compatibilidades entre as arestas
     *
     * @param graph - grafo original
     *
     */
//*************************************************************************************************************     
    public static void initializeCompatibility(Graph graph) {

        //System.out.println("Gerando matrizes de dados...");
        Compatibility c = new Compatibility();
        compatibility = new double[graph.getSizeEdge()][graph.getSizeEdge()];
        double individualCompatibility = 0;

        ListIterator<Edge> listIterator1 = graph.getEdgeList().listIterator();
        while (listIterator1.hasNext()) {
            Edge edge1 = listIterator1.next();

            ListIterator<Edge> listIterator2 = graph.getEdgeList().listIterator();
            while (listIterator2.hasNext()) {
                Edge edge2 = listIterator2.next();

                if (edge1 == edge2) {
                    individualCompatibility = 1;
                } else {
                    //Node centerNode;
                    //centerNode = edge1.commonNode(edge2);

                    //if (centerNode == null) {
                    //    individualCompatibility = 0;
                    //} else {
                        /*Node node1 = new Node();
                        Node node2 = new Node();

                        if (edge1.getStartNode() != centerNode) {
                            node1 = edge1.getStartNode();
                        } else {
                            node1 = edge1.getEndNode();
                        }

                        if (edge2.getStartNode() != centerNode) {
                            node2 = edge2.getStartNode();
                        } else {
                            node2 = edge2.getEndNode();
                        }*/

                        //Point.Double p = new Point.Double(centerNode.getX(), centerNode.getY());
                        //Point.Double p1 = new Point.Double(node1.getX(), node1.getY());
                        //Point.Double p2 = new Point.Double(node2.getX(), node2.getY());
                        //System.out.println("Aqui1");
                        individualCompatibility = c.totalCompatibility(edge1, edge2);

                    //}
                }
                compatibility[edge1.getId()][edge2.getId()] = individualCompatibility;
            }
        }
        if (Flags.isDATATABLE()) {
            save("Compatibility.txt", compatibility);
        }
    }

    //*************************************************************************************************************
    /**
     * Inicializada as matrizes (angulo, espaco, visibilidade, posicao e
     * compatibilidade)
     *
     * @param graph - grafo original
     *
     */
//*************************************************************************************************************     
    public static void initializeData(Graph graph) {

        graph.nodesOfBound();
        
        Compatibility c = new Compatibility();

        angles = new double[graph.getSizeEdge()][graph.getSizeEdge()];

        compatibilityAngular = new double[graph.getSizeEdge()][graph.getSizeEdge()];
        compatibilityScale = new double[graph.getSizeEdge()][graph.getSizeEdge()];
        compatibilityPosition = new double[graph.getSizeEdge()][graph.getSizeEdge()];
        compatibilityVisibility = new double[graph.getSizeEdge()][graph.getSizeEdge()];
        compatibilityDistance = new double[graph.getSizeEdge()][graph.getSizeEdge()];

        compatibility = new double[graph.getSizeEdge()][graph.getSizeEdge()];

        double individualAngle = -1;

        double individualCompatibilityAngular = 0;
        double individualCompatibilityScale = 0;
        double individualCompatibilityPosition = 0;
        double individualCompatibilityVisibility = 0;
        double individualCompatibilityDistance = 0;

        double individualCompatibility = 0;

        ListIterator<Edge> listIterator1 = graph.getEdgeList().listIterator();
        while (listIterator1.hasNext()) {
            Edge edge1 = listIterator1.next();

            ListIterator<Edge> listIterator2 = graph.getEdgeList().listIterator();
            while (listIterator2.hasNext()) {
                Edge edge2 = listIterator2.next();

                if (edge1 == edge2) {
                    individualAngle = -1;

                    individualCompatibilityAngular = 1;
                    individualCompatibilityScale = 1;
                    individualCompatibilityPosition = 1;
                    individualCompatibilityVisibility = 1;
                    individualCompatibilityDistance = 1;
                    individualCompatibility = 1;
                } else {
                    /*Node centerNode;
                    centerNode = edge1.commonNode(edge2);

                    if (centerNode == null) {
                        individualAngle = -1;
                        individualCompatibility = 0;

                        individualCompatibilityAngular = 0;
                        individualCompatibilityScale = 0;
                        individualCompatibilityPosition = 0;
                        individualCompatibilityVisibility = 0;
                    } else {
                        Node node1 = new Node();
                        Node node2 = new Node();

                        if (edge1.getStartNode() != centerNode) {
                            node1 = edge1.getStartNode();
                        } else {
                            node1 = edge1.getEndNode();
                        }

                        if (edge2.getStartNode() != centerNode) {
                            node2 = edge2.getStartNode();
                        } else {
                            node2 = edge2.getEndNode();
                        }

                        Point.Double p = new Point.Double(centerNode.getX(), centerNode.getY());
                        Point.Double p1 = new Point.Double(node1.getX(), node1.getY());
                        Point.Double p2 = new Point.Double(node2.getX(), node2.getY());

                        individualAngle = Geometric.angle(p1, p, p2, p);*/

                    
                        Node node1 = new Node();
                        node1 = edge1.getStartNode();                      
                        Node node2 = new Node();
                        node2 = edge1.getEndNode();
                        
                        Node node3 = new Node();
                        node3 = edge2.getStartNode();                      
                        Node node4 = new Node();
                        node4 = edge2.getEndNode();
                        Point.Double p1 = new Point.Double(node1.getX(), node1.getY());
                        Point.Double p2 = new Point.Double(node2.getX(), node2.getY());
                        Point.Double p3 = new Point.Double(node3.getX(), node3.getY());
                        Point.Double p4 = new Point.Double(node4.getX(), node4.getY());

                        individualAngle = Geometric.angleBetween2Lines(p1, p2, p3, p4);
                        //individualAngle = Geometric.angle(p1, p2, p3, p4);
                        //System.out.println("\nAresta 1: " + edge1.getId() +  " Aresta 2: " + edge2.getId());
                        
                        individualCompatibility = c.totalCompatibility(edge1, edge2);
                        individualCompatibilityAngular = c.angular(edge1, edge2);
                        individualCompatibilityScale = c.scale(edge1, edge2);
                        individualCompatibilityPosition = c.position(edge1, edge2);
                        individualCompatibilityVisibility = c.visibility(edge1, edge2);
                        individualCompatibilityDistance = c.distance(edge1, edge2);

                    //}
                }
                angles[edge1.getId()][edge2.getId()] = individualAngle;

                compatibility[edge1.getId()][edge2.getId()] = individualCompatibility;

                compatibilityAngular[edge1.getId()][edge2.getId()] = individualCompatibilityAngular;
                compatibilityScale[edge1.getId()][edge2.getId()] = individualCompatibilityScale;
                compatibilityPosition[edge1.getId()][edge2.getId()] = individualCompatibilityPosition;
                compatibilityVisibility[edge1.getId()][edge2.getId()] = individualCompatibilityVisibility;
                compatibilityDistance[edge1.getId()][edge2.getId()] = individualCompatibilityDistance;
            }
        }

        if (Flags.isDATATABLE()) {
            save("Angle.txt", angles);
            save("AngularCompatibility.txt", compatibilityAngular);
            save("ScaleCompatibility.txt", compatibilityScale);
            save("PositionCompatibility.txt", compatibilityPosition);
            save("VisibilityCompatibilyty.txt", compatibilityVisibility);
            save("DistanceCompatibilyty.txt", compatibilityDistance);
            save("Compatibility.txt", compatibility);
        }
    }

    public static void save(String file, double[][] matrix) {
        try{
            
            Log log = new Log();
            String msgLog = "";
            log.create(file, false);

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    msgLog += matrix[i][j] + "\t";
                }
                msgLog += "\n";
            }

            log.save(msgLog);
            log.close();
            
        }catch(Exception e){
            System.out.println("Erro do salvamento das tabelas de compatibilidade.");
        }
    }

    public static double getCompatibility(int id1, int id2) {
        return compatibility[id1][id2];
    }

    public static double getAngles(int id1, int id2) {
        return (double) angles[id1][id2];
    }

    public static double getCompatibilityAngular(int id1, int id2) {
        return compatibilityAngular[id1][id2];
    }

    public static double getCompatibilityScale(int id1, int id2) {
        return compatibilityScale[id1][id2];
    }

    public static double getCompatibilityPosition(int id1, int id2) {
        return compatibilityPosition[id1][id2];
    }

    public static double getCompatibilityVisibility(int id1, int id2) {
        return compatibilityVisibility[id1][id2];
    }

    public static double[][] getCompatibilityAngular() {
        return compatibilityAngular;
    }

    public static double[][] getCompatibilityScale() {
        return compatibilityScale;
    }

    public static double[][] getCompatibilityPosition() {
        return compatibilityPosition;
    }

    public static double[][] getCompatibilityVisibility() {
        return compatibilityVisibility;
    }

    public static double[][] getCompatibility() {
        return compatibility;
    }

    public static int[][] getAdjacency() {
        return adjacency;
    }
    
    public static boolean isAdjacent(int index1, int index2) {
        
        if (adjacency[index1][index2] == 0)
            return false;
        
        return true;
    }
    
    public static int[][] getCommomNode() {
        return commomNode;
    }
    
    public static int getCommomNode(int index1, int index2) {
        return commomNode[index1][index2];
    }

    /**
     * @return the compatibilityDistance
     */
    public static double getCompatibilityDistance(int id1, int id2) {
         return compatibilityDistance[id1][id2];
    }
}
