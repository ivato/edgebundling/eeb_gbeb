package edge.bundling;

import edge.bundling.util.Flags;
import edge.bundling.view.ViewMainWindow;
import javax.swing.*;

class MainWindow extends JFrame {

    private static final long serialVersionUID = 1L;

    public MainWindow() {
        ViewMainWindow screen = new ViewMainWindow();
        getContentPane().add(screen);
        setTitle(Flags.TITLE());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setMinimumSize(this.getSize());
        pack();
    }
}
