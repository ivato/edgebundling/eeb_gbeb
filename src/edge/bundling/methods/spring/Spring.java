package edge.bundling.methods.spring;

import edge.bundling.graph.Graph;
import edge.bundling.util.Flags;

public class Spring {

   /* public double xSpringMethodForce = 0;
    public double xRepulsionForce = 0;
    public double ySpringMethodForce = 0;
    public double yRepulsionForce = 0;

    double xForce = 0.0, yForce = 0.0;

    public double stiffness = 10;
    public double springMethodLength = 10;
    public double electricalRepulsion = 100;

    public Graph graphSpring = new Graph(false);

    public double xMin = 10;
    public double xMax;
    public double yMin = 10;
    public double yMax;

    private double xNew;
    private double yNew;

    //*************************************************************************************************************
    /// <summary>
    ///	Implementa o algoritimo de força
    /// </summary>
    //*************************************************************************************************************
    public void run(Graph graph) {
        double yIncrement = 0.0;
        double xIncrement = 0.0;

        int count = 1;

        Crossing crossing = new Crossing();
        crossing.calculate(graph);

        xMax = Flags.getxSizeScreen();
        yMax = Flags.getySizeScreen();

        while (count <= 1000) {
            //Calcula a força de todos
            for (int j = 0; j < graph.getSizeNode(); j++) {
                relax(graph, j);
                graph.getNodeList().get(j).setxForce(xForce);
                graph.getNodeList().get(j).setyForce(yForce);

                xIncrement = 0 - ((0.5) * graph.getNodeList().get(j).getxForce());
                yIncrement = 0 - ((0.5) * graph.getNodeList().get(j).getyForce());

                xNew = graph.getNodeList().get(j).getX() + xIncrement + Flags.getScaleDefault();
                yNew = graph.getNodeList().get(j).getX() + yIncrement + Flags.getScaleDefault();
                
                //AjustBoundPosition(xIncrement, yIncrement);
               
                graph.getNodeList().get(j).setX(xNew);
                graph.getNodeList().get(j).setY(yNew);
            }
          
            crossing.calculate(graph);
            count++;
        }
    }

    //*************************************************************************************************************
    /// <summary>
    /// Calcula a força em um vértice do grafo
    /// </summary>
    /// <param name="graph">Grafo no qual será calculada a força</param>
    /// <param name="idVertice">ID do vértice para o qual será calculada a força</param>
//*************************************************************************************************************
    private void relax(Graph graph, int idVertice) {
        double distance;

        double xAdjacentVertice;
        double yAdjacentVertice;
        double xOtherVertice;
        double yOtherVertice;
        double xIdVertice;
        double yIdVertice;

        double dx, dy;
        double delta;

        int adjacentVerticeLength = 0;
        int verticeLength = 0;

        xIdVertice = graph.getNodeList().get(idVertice).getX();
        yIdVertice = graph.getNodeList().get(idVertice).getY();

        xSpringMethodForce = 0;
        xRepulsionForce = 0;
        ySpringMethodForce = 0;
        yRepulsionForce = 0;

        //Calcula força de atração do vertice
        adjacentVerticeLength = graph.getNodeList().get(idVertice).getAdjNodeList().size();
        for (int i = 0; i < adjacentVerticeLength; i++) {
            xAdjacentVertice = graph.getNodeList().get(idVertice).getAdjNodeList().get(i).getX();
            yAdjacentVertice = graph.getNodeList().get(idVertice).getAdjNodeList().get(i).getY();

            dx = xIdVertice - xAdjacentVertice;
            dy = yIdVertice - yAdjacentVertice;
            delta = (dx * dx) + (dy * dy);

            distance = Math.sqrt(delta);
            if (distance == 0) {
                distance = 0.0001;
            }

            xSpringMethodForce += stiffness * Math.log(distance / springMethodLength) * ((xIdVertice - xAdjacentVertice) / (distance));
            ySpringMethodForce += stiffness * Math.log(distance / springMethodLength) * ((yIdVertice - yAdjacentVertice) / (distance));

        }

        //Calcula força de repulsão do vertice
        verticeLength = graph.getSizeNode();
        for (int i = 0; i < verticeLength; i++) {
            if (idVertice != i) {
                xOtherVertice = graph.getNodeList().get(i).getX();
                yOtherVertice = graph.getNodeList().get(i).getY();

                dx = xIdVertice - xOtherVertice;
                dy = yIdVertice - yOtherVertice;
                delta = (dx * dx) + (dy * dy);
                distance = Math.sqrt(delta);
                if (distance == 0) {
                    distance = 0.0001;
                }

                xRepulsionForce += (electricalRepulsion / distance) * ((xIdVertice - xOtherVertice) / (distance));
                yRepulsionForce += (electricalRepulsion / distance) * ((yIdVertice - yOtherVertice) / (distance));
            }
        }

        //Seta as forças para o vértice
        xForce = xSpringMethodForce - xRepulsionForce;
        yForce = ySpringMethodForce - yRepulsionForce;
    }

    //*************************************************************************************************************
    /// <summary>
    /// Procedimento que deixa a posição do nó limitados a janela
    /// </summary>
    /// <param name="xIncrement">Incremento X de movimento do nó</param>
    /// <param name="yIncrement">Incremento Y de movimento do nó</param>
    //*************************************************************************************************************
    private void AjustBoundPosition(double xIncrement, double yIncrement) {
        if (xNew < xMin) {
            if (xIncrement > 0) {
                xNew = xMin + xIncrement;
            } else {
                xNew = xMin - xIncrement;
            }
        } else if (xNew > xMax) {
            if (xIncrement > 0) {
                xNew = xMax - xIncrement;
            } else {
                xNew = xMax + xIncrement;
            }
        }

        if (yNew < yMin) {
            if (yIncrement > 0) {
                yNew = yMin + yIncrement;
            } else {
                yNew = yMin - yIncrement;
            }
        } else if (yNew > yMax) {
            if (yIncrement > 0) {
                yNew = yMax - yIncrement;
            } else {
                yNew = yMax + yIncrement;
            }
        }
    }*/
}
