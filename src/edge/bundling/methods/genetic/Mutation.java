package edge.bundling.methods.genetic;

import edge.bundling.graph.Bundle;
import edge.bundling.graph.Constraints;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import edge.bundling.util.Log;
import edge.bundling.util.Message;
import java.util.ListIterator;
import java.util.Random;

public class Mutation {

    //*************************************************************************************************************
    /**
     * Executa a mutação do indivíduo
     *
     * @param child - indivíduo a ser mutado
     */
//*************************************************************************************************************     
    public void mutation(Individual child) throws Exception {
        String msgLog = "";
        Log log = new Log();
        int flag = 0;

        Random rndSeed = new Random(System.currentTimeMillis());
        int seed = rndSeed.nextInt(19700621);
        Random rnd = new Random(seed);
        float num = (float) rnd.nextFloat();

        if (num < Flags.MUTATION_RATIO()) {

            if (Flags.isAUTOMATIC_SAVING()) {
                log.create("EvolutionProcess.txt", true);

                msgLog += "\nMUTACAO";
                msgLog += Message.individialDataFile(child, false, "Filho Original");

            }

            rnd = new Random(System.currentTimeMillis());
            int number = (int) rnd.nextInt(6);

            if ((number == 1) && Flags.isMUTATION_JOIN()) {
                try {
                    if (joinMutation(child)) {
                        msgLog += "\n...com mutação 1...\n";
                        flag = 1;
                    }
                } catch (Exception e) {
                    System.out.println("Erro no processo de mutação 1");
                    System.out.println(e.getMessage());
                    System.out.println(e.getCause());
                }
            } else if ((number == 2) && Flags.isMUTATION_SPLIT()) {
                try {
                    if (splitMutation(child)) {
                        msgLog += "\n...com mutação 2...\n";
                        flag = 1;
                    }
                } catch (Exception e) {
                    System.out.println("Erro no processo de mutação 2");
                    System.out.println(e.getMessage());
                    System.out.println(e.getCause());
                }
            } else if ((number == 3) && Flags.isMUTATION_MOVE()) {
                try {
                    if (moveMutation(child)) {
                        msgLog += "\n...com mutação 3...\n";
                        flag = 1;
                    }
                } catch (Exception e) {
                    System.out.println("Erro no processo de mutação 3");
                    System.out.println(e.getMessage());
                    System.out.println(e.getCause());
                }
            } else if ((number == 4) && Flags.isMUTATION_MERGE()) {
                try {
                    if (mergeMutation(child)) {
                        msgLog += "\n...com mutação 4...\n";
                        flag = 1;
                    }
                } catch (Exception e) {
                    System.out.println("Erro no processo de mutação 4");
                    System.out.println(e.getMessage());
                    System.out.println(e.getCause());
                }
            } else if ((number == 5) && Flags.isMUTATION_REMOVE()) {
                try {
                    if (removeMutation(child)) {
                        msgLog += "\n...com mutação 5...\n";
                        flag = 1;
                    }
                } catch (Exception e) {
                    System.out.println("Erro no processo de mutação 5");
                    System.out.println(e.getMessage());
                    System.out.println(e.getCause());
                }
            }

            if ((flag == 1) && (Flags.isAUTOMATIC_SAVING())) {

                msgLog += Message.individialDataFile(child, false, "Filho Mutado");

                log.save(msgLog);
                log.close();
            }

        }
    }

 
        
    //*************************************************************************************************************
    /**
     * Monta um novo feixe com duas arestas que estavam sozinhas
     *
     * @param sizeEdge - Quantidade de arestas do grafo que gerou o indivíduo
     * @param individual - Indivíduo que sofrerá mutação
     * @return boolean
     */
//************************************************************************************************************* 
    public boolean joinMutation(Individual individual) {
        int index, index1 = -1, index2 = -1;
        int count = 0;

        //Procura no indivíduo dois feixes com uma única aresta
        //Esta pegando os dois primeiros
        ListIterator<Bundle> listIterator = individual.getBundleList().listIterator();
        while (listIterator.hasNext()) {
            Bundle bundle = new Bundle();
            index = listIterator.nextIndex();
            bundle = listIterator.next();
            if ((bundle.getEdges().size() == 1) && (count == 0)) {
                index1 = index;
                count++;
            } else if ((bundle.getEdges().size() == 1) && (count == 1)) {
                index2 = index;
                count++;
                break;
            }
        }

        //Caso tenha encontrado dois feixes com apenas uma aresta
        if ((index1 != -1) && (index2 != -1)) {
            Bundle bundle1 = new Bundle();
            Bundle bundle2 = new Bundle();

            bundle1 = individual.getBundleList().get(index1);
            bundle2 = individual.getBundleList().get(index2);

            Edge e1 = new Edge();
            Edge e2 = new Edge();
            e1 = individual.getBundleList().get(index1).getEdges().get(0);
            e2 = individual.getBundleList().get(index2).getEdges().get(0);

            individual.remove(bundle1);
            individual.remove(bundle2);

            Bundle newBundle = new Bundle();
            newBundle.add(e1);
            newBundle.add(e2);

            individual.add(newBundle);
      
            return true;
        }
        return false;
    }

    //*************************************************************************************************************
    /**
     * Divide um feixe em dois novos feixes
     *
     * @param sizeEdge - Quantidade de arestas do grafo que gerou o indivíduo
     * @param individual - Indivíduo que sofrerá mutação
     * @return boolean
     */
//*************************************************************************************************************
    private boolean splitMutation(Individual individual) {
        Random rnd;
        int index, limSup = 0, limInf = 0;
        int numberOfEdges;

        int numberOfBundles = individual.getNumberOfBundles();
        if (numberOfBundles > 1) {

            //Recupera de forma aleatória um dos feixes do grafo
            rnd = new Random(System.currentTimeMillis());
            index = (int) rnd.nextInt(numberOfBundles - 1);
            Bundle bundle = new Bundle();
            bundle = individual.getBundleList().get(index);

            numberOfEdges = bundle.getSize();
            if (numberOfEdges > 1) {

                //Define o ponto de divisão das arestas de forma aleatória
                Random rndSeed = new Random(System.currentTimeMillis());
                int seed = rndSeed.nextInt(19700621);
                rnd = new Random(seed);
                index = (int) rnd.nextInt(numberOfEdges - 1);

                //Cria dois novos feixes
                Bundle bundle1 = new Bundle();
                Bundle bundle2 = new Bundle();

                if (index == 0) {
                    limInf = 0;
                    limSup = 1;
                } else {
                    if (index == (numberOfEdges - 1)) {
                        limInf = numberOfEdges - 2;
                        limSup = numberOfEdges - 1;
                    } else {
                        limInf = index;
                        limSup = index + 1;
                    }
                }

                //Coloca o primeiro grupo de arestas no feixe 1
                for (int i = 0; i <= limInf; i++) {
                    Edge edge = new Edge();
                    edge = bundle.getEdges().get(i);
                    bundle1.add(edge);
                }

                //Coloca o segundo grupo de arestas no feixe 2
                for (int i = limSup; i < numberOfEdges; i++) {
                    Edge edge = new Edge();
                    edge = bundle.getEdges().get(i);
                    bundle2.add(edge);
                }

                //Remove o feixe antido e inclui os dois novos feixes
                individual.remove(bundle);
                individual.add(bundle1);
                individual.add(bundle2);

                return true;
            }
        }
        return false;
    }

    //*************************************************************************************************************
    /**
     * Retira uma aresta de um feixe e a coloca em outro feixe 
     *
     * @param sizeEdge - Quantidade de arestas do grafo que gerou o indivíduo
     * @param individual - Indivíduo que sofrerá mutação
     * @return boolean
     */
//*************************************************************************************************************
    private boolean moveMutation(Individual individual) {
        Random rnd;
        float num;
        int number;

        //Recupera aleatoriamente um feixe do individuo de tamanho maior do que 1
        Random rndSeed = new Random(System.currentTimeMillis());
        int seed = rndSeed.nextInt(19700621);
        rnd = new Random(seed);
        number = (int) rnd.nextInt(individual.getNumberOfBundles());

        Bundle bundle = (Bundle) individual.get(number);

        //Retira aleatoriamente uma aresta deste feixe
        Edge edge1 = new Edge();
        if (bundle.getEdges().size() > 1) {

            rndSeed = new Random(System.currentTimeMillis());
            seed = rndSeed.nextInt(19700621);
            rnd = new Random(seed);
            number = (int) rnd.nextInt(bundle.getEdges().size());

            edge1 = bundle.getEdges().get(number);
            bundle.remove(edge1);

            
            rndSeed = new Random(System.currentTimeMillis());
            seed = rndSeed.nextInt(45700621);
            rnd = new Random(seed);
            int index = (int) rnd.nextInt(individual.getBundleList().size());
            

            individual.getBundleList().get(index).add(edge1);
            individual.getBundleList().get(index).evaluate();
            
            return true;
        }
        return false;
    }

    //*************************************************************************************************************
    /**
     * Une dois feixes 
     *
     * @param sizeEdge - Quantidade de arestas do grafo que gerou o indivíduo
     * @param individual - Indivíduo que sofrerá mutação
     * @return boolean
     */
//*************************************************************************************************************
    private boolean mergeMutation(Individual individual) {
        Random rnd;
        float num;
        int number;
        int flag = 0;

        //Recupera aleatoriamente dois feixes do individuo 
        Bundle bundle1 = new Bundle();
        Random rndSeed = new Random(System.currentTimeMillis());
        int seed = rndSeed.nextInt(19700621);
        rnd = new Random(seed);
        number = (int) rnd.nextInt(individual.getNumberOfBundles());
        bundle1 = (Bundle) individual.get(number);

        Bundle bundle2 = new Bundle();
        rndSeed = new Random(System.currentTimeMillis());
        seed = rndSeed.nextInt(19700621);
        rnd = new Random(seed);
        number = (int) rnd.nextInt(individual.getNumberOfBundles());
        bundle2 = (Bundle) individual.get(number);
        
        if (bundle1.getId() != bundle2.getId()){
            flag = 1;
        }

        //Caso tenha encontrado passa todas as arestas para o feixe 1 e apaga o feixe 2
        if (flag == 1) {
            ListIterator<Edge> edgeIterator = bundle2.getEdges().listIterator();
            while (edgeIterator.hasNext()) {
                Edge edge = new Edge();
                edge = edgeIterator.next();
                bundle1.add(edge);
            }
            individual.remove(bundle2);
            bundle1.evaluate();
            return true;
        }
        return false;
    }

    //*************************************************************************************************************
    /**
     * Retira a aresta de um feixe e monta um feixe com ela
     *
     * @param sizeEdge - Quantidade de arestas do grafo que gerou o indivíduo
     * @param individual - Indivíduo que sofrerá mutação
     * @return boolean
     */
//*************************************************************************************************************
    private boolean removeMutation(Individual individual) {
        Random rnd;
        float num;
        int number;
        int flag = 0;

        //Recupera aleatoriamente um feixe do individuo 
        Random rndSeed = new Random(System.currentTimeMillis());
        int seed = rndSeed.nextInt(19700621);
        rnd = new Random(seed);
        number = (int) rnd.nextInt(individual.getNumberOfBundles());

        Bundle bundle = (Bundle) individual.get(number);

        if (bundle.getSize() > 1) {

            //Recupera aleatoriamente uma aresta deste feixe
            rndSeed = new Random(System.currentTimeMillis());
            seed = rndSeed.nextInt(19700621);
            rnd = new Random(seed);
            number = (int) rnd.nextInt(bundle.getSize());

            Edge edge = (Edge) bundle.getEdges().get(number);

            //Retira a aresta do feixe
            bundle.remove(edge);
            bundle.evaluate();

            //Cria um novo feixe e adiciona a aresta
            Bundle newBundle = new Bundle();
            newBundle.add(edge);
            individual.add(newBundle);

            return true;
        }
        return false;
    }
}
