package edge.bundling.methods.genetic;

import edge.bundling.statistics.GlobalStatistic;
import edge.bundling.statistics.LocalStatistic;
import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundledComparable;
import edge.bundling.graphics.Generator;
import edge.bundling.util.Flags;
import edge.bundling.util.Log;
import edge.bundling.util.Message;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public final class Genetic {

    private Population currentPopulation1 = new Population();
    private Population currentPopulation2 = new Population();
    //private Population currentPopulation3 = new Population();

    private Population bestIndividualsPopulation = new Population();
    private Population bestIndividualsPopulation1 = new Population();
    private Population bestIndividualsPopulation2 = new Population();
    //private Population bestIndividualsPopulation3 = new Population();

    private Population initialPopulation1 = new Population();
    private Population initialPopulation2 = new Population();
    //private Population initialPopulation3 = new Population();
    
    private Graph graph;
    private Individual bestIndividual = new Individual();
    private Individual worstIndividual = new Individual();
    private String statistics = "";
    private int diversityStep = 1;
    private String angle = "";

    private Log logEvolutionIndividual = new Log();
    private Log logEvolution = new Log();
    private String parameters = "";
    private int testNumber;

    public Genetic() {

    }

    public Genetic(Graph graph, int t, String a) throws Exception {

       // System.out.println("Executando processo evolucionário...");
       

        this.angle = a;
        this.testNumber = t;

        this.currentPopulation1 = new Population();
        this.currentPopulation2 = new Population();
        //this.currentPopulation3 = new Population();
        
        this.graph = graph;

        if (Flags.isLOG()) {
            Flags.STATISTICS(true);
            logEvolution.create(this.angle + " - " + Flags.NAMEFILE() + " - DataOfExperiments.txt", true);
            logEvolution.start();
        }

        // dateFormat = new SimpleDateFormat("HH:mm:ss");
       // Date date1 = new Date();
       // Date date2 = new Date();
        //System.out.print("Começa inicializar 1:" + dateFormat.format(date1)); 
        
        this.currentPopulation1.initialize(graph);
        
      //  date2= new Date();
      //  System.out.println("  - finaliza 1:" + dateFormat.format(date2)); 
        
      //  date1= new Date();       
      //  System.out.print("Começa inicializar 2:" + dateFormat.format(date1)); 
        this.currentPopulation2.initialize(graph);
        
      //  date2= new Date();
      //  System.out.println("  - finaliza 2:" + dateFormat.format(date2)); 
        //this.currentPopulation3.initialize(graph);

       // date1= new Date();
       // System.out.print("Começa clonar 1:" + dateFormat.format(date1)); 
        this.initialPopulation1 = this.currentPopulation1.clone();
        
       // date2= new Date();
       // System.out.println("  - finaliza clone 2:" + dateFormat.format(date2)); 
        //this.currentPopulation3.initialize(graph);
        
      //  date1= new Date();
       // System.out.print("Começa clonar 2:" + dateFormat.format(date1)); 
        this.initialPopulation2 = this.currentPopulation2.clone();
        
      //  date2= new Date();
       // System.out.println("  - finaliza clone 2:" + dateFormat.format(date2)); 
        
        //this.currentPopulation3.initialize(graph);
        //this.initialPopulation3 = this.currentPopulation3.clone();
        
        parameters = getParamenters(testNumber);
    }


    //*************************************************************************************************************
    /**
     * Inicia a execução
     *
     * @param graph - Grafo no qual será executado algoritmo genético
     */
//*************************************************************************************************************     
    public void run(Graph graph) {
        try {

            this.run();

            if (Flags.isLOG()) {
                
              
                //logEvolution.finish(parameters, Message.generateDataLog(bestIndividualsPopulation.getConvergenceGeneration(), 
                //        currentPopulation1.getSize(), 
                //        bestIndividualsPopulation.getSize(), 
                //        bestIndividualsPopulation.getBestSolution()));
                
                
                Individual individual = getBestIndividual();
                logEvolution.finish(parameters, Message.generateDataLog(individual.getGeneration(), 
                        currentPopulation1.getSize(), 
                        bestIndividualsPopulation1.getSize(), 
                        individual));
            
                
                
                logEvolution.close();

                String name = this.angle + "TESTE" + testNumber + " - " + Flags.NAMEFILE() + " - BEST SOLUTION.txt";
                logEvolutionIndividual.create(name, false);
                logEvolutionIndividual.start();
                //Individual individual = getBestIndividual();
                String bundles = individual.showBundles();
                logEvolutionIndividual.save(bundles);
                logEvolutionIndividual.close();

                Generator gG = new Generator(testNumber);
                gG.evolution(bestIndividualsPopulation, this.angle);
                //gG.solutions(getInitialPopulation1(), getInitialPopulation2(), getInitialPopulation3(),individual, this.angle);
                gG.solutions(getInitialPopulation1(), getInitialPopulation2(),individual, this.angle);
            }
            
        } catch (Exception e) {

        }
    }
    
    //*************************************************************************************************************
    /**
     * Executa o processo evolucionário
     *
     * @param graph - Grafo no qual será executado algoritmo genético
     */
//*************************************************************************************************************     
    private void run() throws Exception {
        
       // DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
      //  Date date1 = new Date();
      //  Date date2 = new Date();

        int generations = 1;
        String temp;
        String msgLog = "";
        Log log = new Log();

         //Guarda o melhor indivíduo das gerações iniciais 
        //date1 = new Date();
      //  System.out.print("Guarda o melhor individuo da geracao 1:" + dateFormat.format(date1));  
        addBestIndividualAtBestPopulation(bestIndividualsPopulation1, currentPopulation1);
      //  date2 = new Date();
     //   System.out.println("  - finaliza guarda 1:" + dateFormat.format(date2));  
        
      //  date1 = new Date();
      //  System.out.print("Guarda o melhor individuo da geracao 2:" + dateFormat.format(date1));  
        addBestIndividualAtBestPopulation(bestIndividualsPopulation2, currentPopulation2);
    //    date2 = new Date();
     //   System.out.println("  - finaliza guarda 2:" + dateFormat.format(date2));  
        //addBestIndividualAtBestPopulation(bestIndividualsPopulation3, currentPopulation3);

        if (Flags.isSTATISTICS()) {
            log.create(this.angle + " - " + Flags.NAMEFILE() + " - StatisticsOfEachGeneration.txt", true);
            statistics += showStatistics(generations);
        }

        ArrayList<Individual> parents = new ArrayList();
        ArrayList<Individual> children = new ArrayList();

        Selection selection = new Selection(2);
        Crossover crossing = new Crossover();
        Mutation mutation = new Mutation();
        Survival survival = new Survival();

        int sizeOfCurrentPopulation = currentPopulation1.getSize();
         
        while (!isTerminationCondition(generations)) {

            generations++;

            ////Recupera o melhor indivíduo da população anterior
           // date1 = new Date();
          //  System.out.print("Guarda o melhor individuo anterior 1:" + dateFormat.format(date1));  
            currentPopulation1.savePreviousBestIndividual(currentPopulation1.getBestSolution().clone());
           // date2 = new Date();
          //  System.out.println("  - finaliza guarda 1:" + dateFormat.format(date2));  
        
          //  date1 = new Date();
         //   System.out.print("Guarda o melhor individuo anterior 2:" + dateFormat.format(date1));  
            currentPopulation2.savePreviousBestIndividual(currentPopulation2.getBestSolution().clone());
         //   date2 = new Date();
        //    System.out.println("  - finaliza guarda 2:" + dateFormat.format(date2));  
            //currentPopulation3.savePreviousBestIndividual(currentPopulation3.getBestSolution().clone());

           // date1 = new Date();
           // System.out.print("Cria " + sizeOfCurrentPopulation + " individuos: " + dateFormat.format(date1));  
            for (int i = 0; i < (sizeOfCurrentPopulation - 1); i++) {
                 
                
                parents = selection.selection(currentPopulation1, i);
                children = crossing.crossover(graph.getEdgeList(), parents);
                children.get(0).evaluate(currentPopulation1);
                children.get(1).evaluate(currentPopulation1);
                mutation.mutation(children.get(0));
                children.get(0).evaluate(currentPopulation1);
                mutation.mutation(children.get(1));
                children.get(1).evaluate(currentPopulation1);
                survival.survival(currentPopulation1, children, parents, generations);
 
                parents = selection.selection(currentPopulation2, i);
                children = crossing.crossover(graph.getEdgeList(), parents);
                children.get(0).evaluate(currentPopulation2);
                children.get(1).evaluate(currentPopulation2);
                mutation.mutation(children.get(0));
                children.get(0).evaluate(currentPopulation2);
                mutation.mutation(children.get(1));
                children.get(1).evaluate(currentPopulation2);
                survival.survival(currentPopulation2, children, parents, generations);
                

                /*parents = selection.selection(currentPopulation3, i);
                children = crossing.crossover(graph.getEdgeList(), parents);
                children.get(0).evaluate(currentPopulation3);
                children.get(1).evaluate(currentPopulation3);
                mutation.mutation(children.get(0));
                children.get(0).evaluate(currentPopulation3);
                mutation.mutation(children.get(1));
                children.get(1).evaluate(currentPopulation3);
                survival.survival(currentPopulation3, children, parents, generations);*/
            }
          //  date2 = new Date();
          //  System.out.println("  - finaliza criar:" + dateFormat.format(date2));  
            
            //Insere o melhor indivíduo da população anterior na população corrente
           //  date1 = new Date();
           // System.out.print("Adiciona o melhor individuo anterior 1:" + dateFormat.format(date1));  
            currentPopulation1.addPreviousBestIndividual();
           // date2 = new Date();
           // System.out.println("  - finaliza adiciona 1:" + dateFormat.format(date2)); 
            
            // date1 = new Date();
           // System.out.print("Adiciona o melhor individuo anterior 2:" + dateFormat.format(date1));        
            currentPopulation2.addPreviousBestIndividual();
          //  date2 = new Date();
          //  System.out.println("  - finaliza adiciona 2:" + dateFormat.format(date2)); 
            //currentPopulation3.addPreviousBestIndividual();

            //Guarda o melhor indivíduo de cada geração corrente
           //  date1 = new Date();
           // System.out.print("Guarda o melhor individuo corrente 1:" + dateFormat.format(date1));  
            addBestIndividualAtBestPopulation(bestIndividualsPopulation1, currentPopulation1);
          //  date2 = new Date();
          //  System.out.println("  - finaliza guarda 1:" + dateFormat.format(date2));  
            
           //   date1 = new Date();
          //  System.out.print("Guarda o melhor individuo corrente 2:" + dateFormat.format(date1));   
            addBestIndividualAtBestPopulation(bestIndividualsPopulation2, currentPopulation2);
          //  date2 = new Date();
          //  System.out.println("  - finaliza guarda 2:" + dateFormat.format(date2));  
            //addBestIndividualAtBestPopulation(bestIndividualsPopulation3, currentPopulation3);
            
            //Transfere a melhor solução entre as três populações
            //addBestIndividualAtCurrentPopulations(currentPopulation1, currentPopulation2, currentPopulation3);
          //  date1 = new Date();
           // System.out.print("Guarda o melhor individuo nas populações:" + dateFormat.format(date1));  
            addBestIndividualAtCurrentPopulations(currentPopulation1, currentPopulation2);
           // date2 = new Date();
           // System.out.println("  - finaliza guarda:" + dateFormat.format(date2));  


            // date1 = new Date();
            //System.out.print("Guarda o melhor individuo:" + dateFormat.format(date1));  
            bestIndividualsPopulation.add(getBestIndividual().clone());
           // date2 = new Date();
            //System.out.println("  - finaliza guarda:" + dateFormat.format(date2) +  "\n");  
            
            //System.out.println(getBestIndividual().getFitness() + " - " + bestIndividualsPopulation.getBestSolution().getFitness());
            
            //System.out.println("Statiscing");
            if (Flags.isSTATISTICS()) {
               statistics += showStatistics(generations);
            }
            
            //System.out.println(generations + " - " + diversityStep);
        }
        
        Flags.MAX_GENERATION_FINAL(generations);

        if (Flags.isSTATISTICS()) {
            // AQUI statistics += Message.statistics(getBestIndividualsPopulation().getBestSolution());

            log.save(statistics);
            log.close();
        }

        if (Flags.isAUTOMATIC_SAVING()) {
            Log populationSrt = new Log();
            populationSrt.create("FinalPopulation.txt");
            msgLog = currentPopulation1.showPopulation();
            populationSrt.save(msgLog);
            populationSrt.close();

            populationSrt = new Log();
            populationSrt.create("BestPopulation.txt");
           // AQUI  msgLog = getBestIndividualsPopulation().showPopulation();
            populationSrt.save(msgLog);
            populationSrt.close();
        }
    }

    private void addBestIndividualAtBestPopulation(Population bestIndividualsPopulation, Population currentPopulation) {
        bestIndividualsPopulation.add(currentPopulation.getBestSolution().clone());
    }

    
    //public void addBestIndividualAtCurrentPopulations(Population currentPopulation1, Population currentPopulation2, Population currentPopulation3) {
    public void addBestIndividualAtCurrentPopulations(Population currentPopulation1, Population currentPopulation2) {
    
        Individual individual1 = new Individual();
        individual1 = (Individual) currentPopulation1.getBestSolution();

        Individual individual2 = new Individual();
        individual2 = (Individual) currentPopulation2.getBestSolution();

        //Individual individual3 = new Individual();
        //individual3 = (Individual) currentPopulation3.getBestSolution();

        //if ((individual1.getFitness() > individual2.getFitness()) && (individual1.getFitness() > individual3.getFitness())) {
        if ((individual1.getFitness() > individual2.getFitness())) {
       
            currentPopulation2.removeWorstSolution();
            currentPopulation2.add(individual1);

            //currentPopulation3.removeWorstSolution();
            //currentPopulation3.add(individual1);

        } else {
            //if ((individual2.getFitness() > individual1.getFitness()) && (individual2.getFitness() > individual3.getFitness())) {
            if ((individual2.getFitness() > individual1.getFitness())) {
             
                currentPopulation1.removeWorstSolution();
                currentPopulation1.add(individual2);

                //currentPopulation3.removeWorstSolution();
                //currentPopulation3.add(individual2);

            } //else {
                //if ((individual3.getFitness() > individual1.getFitness()) && (individual3.getFitness() > individual2.getFitness())) {

                   // currentPopulation1.removeWorstSolution();
                   // currentPopulation1.add(individual3);

                   // currentPopulation2.removeWorstSolution();
                   // currentPopulation2.add(individual3);

                //}
           // }
        }
    }

    
    public Individual getBestIndividual() {
        Individual individual1 = new Individual();
        individual1 = (Individual) currentPopulation1.getBestSolution();

        Individual individual2 = new Individual();
        individual2 = (Individual) currentPopulation2.getBestSolution();

       /* Individual individual3 = new Individual();
        individual3 = (Individual) currentPopulation3.getBestSolution();

        if ((individual1.getFitness() > individual2.getFitness()) && (individual1.getFitness() > individual3.getFitness())) {
            return individual1;
        } else {
            if ((individual2.getFitness() > individual1.getFitness()) && (individual2.getFitness() > individual3.getFitness())) {
                return individual2;
            } else {
                return individual3;

            }
        }*/
       
       if ((individual1.getFitness() > individual2.getFitness())) {
            return individual1;
        } else {
           return individual2;
        }
    }
    
    //*************************************************************************************************************
    /**
     * Verifica a condição de parada
     *
     * @param generations - número de gerações
     * @return boolean
     */
//*************************************************************************************************************     
    public boolean isTerminationCondition(int generations) {

        
        GraphBundledComparable gc = new GraphBundledComparable();       
        
        if (generations > 16500){
            return true;
        }
        
        
        //Continua o processo evolucionário por um número fixo de gerações
        if (Flags.isTERMINATION_BY_GENERATION()) {
            bestIndividualsPopulation.setConvergenceGeneration(getBestIndividual().getGeneration());
            
            int limit = (int) (Flags.MAX_GENERATION());
            if (generations < limit) {
                return false;
            }
        }
        
        //Continua o processo evolucionário enquanto ocorrem mudanças
        if (Flags.isTERMINATION_BY_DIVERSITY()) {
            bestIndividualsPopulation.setConvergenceGeneration(getBestIndividual().getGeneration());
            
            int test;
            test = gc.compare(currentPopulation1.getBestSolution(),currentPopulation1.getPreviousBestIndividual());
 
            //if (test != 0) {
            //Se melhorou
            if (test == -1) {
                diversityStep = 1;
                return false;
            } else {
                //Se é igual ou pior
                diversityStep++;
                if (diversityStep > Flags.MAX_GENERATION_TO_WAIT()) {
                    return true;
                } else {
                    return false;
                }
            }
        }


        return true;
    }

    //*************************************************************************************************************
    /**
     * Retorna os dados da melhor solução final
     *
     * @return best - dados da melhor solução
     */
//*************************************************************************************************************     
    public String showBestSolution() {
        String best = "";

        //best += "Geração de convergência: " + bestIndividualsPopulation.getConvergenceGeneration() + "\n";
        //best += "Quantidade de indivíduos na população corrente: " + currentPopulation1.getSize() + "\n";

        //Individual bestSolution = getBestIndividualOfBestIndividualsPopulation();
        
        Individual bestIndividual = new Individual();
        bestIndividual = getBestIndividual();
        best += Message.individialData(bestIndividual, false, "");
        return best;
    }

    //*************************************************************************************************************
    /**
     * Retorna os dados da melhor solução sem os feixes
     *
     * @return best - dados da melhor solução
     */
//*************************************************************************************************************     
    private String showStatistics(int generations) {

        String best = "";
        GlobalStatistic globalData = currentPopulation1.getGlobalData();
        LocalStatistic localData = new LocalStatistic();
        localData.setSumLocalFitness(currentPopulation1.getSumLocalFitness());
        localData.setQuantityLocalIndividuals(currentPopulation1.getIndividuals().size());

        Individual bestSolution = currentPopulation1.getBestSolution();
        Individual worstSolution = currentPopulation1.getWorstSolution();
        best += Message.statistics(generations, bestSolution, worstSolution, localData, globalData, false);
        bestIndividual = bestSolution.clone();
        worstIndividual = worstSolution.clone();

        return best;
    }

    public String getParamenters(int testNumber) {
        String log = "";
        log += testNumber + "\t";

        log += graph.getSizeNode() + "\t";

        log += graph.getSizeEdge() + "\t";

        log += Flags.NAMEFILE() + "\t";

        if (Flags.isEB_STAR()) {
            log += "S\t";
        } else {
            log += "N\t";
        }

        if (Flags.isANGULAR()) {
            log += " A ";
        }
        if (Flags.isSCALE()) {
            log += " S ";
        }
        if (Flags.isPOSITION()) {
            log += " P ";
        }
        if (Flags.isVISIBILITY()) {
            log += " V ";
        }
        log += "\t";

        if (Flags.isOBJECTIVE()) {
            log += "O\t";
        }

        if (Flags.isCONSTRAINT()) {
            log += "C\t";
        }

        log += Flags.TAM_POPULATION() + "\t";
        log += this.initialPopulation1.getIndividuals().size() + "\t";
        log += Flags.MAX_GENERATION_TO_WAIT() + "\t";
        log += Flags.CROSSOVER_RATIO() + "\t";

        if (Flags.isONE_POINT()) {
            log += "P\t";
        }

        if (Flags.isUNIFORM()) {
            log += "U\t";
        }

        log += Flags.MUTATION_RATIO() + "\t";

        if (Flags.isMUTATION_JOIN()) {
            log += " 1 ";
        }

        if (Flags.isMUTATION_SPLIT()) {
            log += " 2 ";
        }

        if (Flags.isMUTATION_MOVE()) {
            log += " 3 ";
        }

        if (Flags.isMUTATION_MERGE()) {
            log += " 4 ";
        }

        if (Flags.isMUTATION_REMOVE()) {
            log += " 5 ";
        }

        log += "\t";

        if (Flags.isTERMINATION_BY_GENERATION()) {
            log += "G\t";
        }

        if (Flags.isTERMINATION_BY_DIVERSITY()) {
            log += "D\t";
        }

        if (Flags.isROULETTE_WHEEL()) {
            log += "RW\t";
        }
        if (Flags.isRANKING()) {
            log += "RK\t";
        }
        if (Flags.isRANDOM()) {
            log += "RD\t";
        }
        if (Flags.isTOURNAMENT()) {
            log += "T\t";
        }

        log += Flags.SELECTIVE_PRESSURE() + "\t";

        return log;
    }

    public Population getCurrentPopulation1() {
        return currentPopulation1;
    }

    public Population getInitialPopulation1() {
        return initialPopulation1;
    }
    
     public Population getInitialPopulation2() {
        return initialPopulation2;
    }
     
    //  public Population getInitialPopulation3() {
    //    return initialPopulation3;
    //}

    public Population getBestIndividualsPopulation1() {
        return bestIndividualsPopulation1;
    }
    
    public Population getBestIndividualsPopulation2() {
        return bestIndividualsPopulation2;
    }
     
   // public Population getBestIndividualsPopulation3() {
   //     return bestIndividualsPopulation3;
   // }

}
