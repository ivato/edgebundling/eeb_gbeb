package edge.bundling.methods.genetic;

import edge.bundling.graph.GraphBundledComparable;
import edge.bundling.util.Flags;
import edge.bundling.util.Log;
import java.util.ArrayList;
import java.util.Collections;

public class Survival {

    private Population population;

    public void survival(Population population, ArrayList<Individual> children, ArrayList<Individual> parents, int generations) {
        this.population = population;

        //if (Flags.isTERMINATION_BY_GENERATION()) {
            Survival.this.insertChildren(children, generations);
        //}

        //if (Flags.isTERMINATION_BY_DIVERSITY()) {
        //    insertChildren(children, parents, generations);
        //}
    }

    //*************************************************************************************************************
    /**
     * Responsável por alterar a população com o novos indivíduos Insere os
     * filhos na população e depois retira os dois piores
     *
     * @param childrenList - Lista de indivíduos filhos
     */
//*************************************************************************************************************    
    public void insertChildren(ArrayList<Individual> childrenList, int generation) {
        String msgLog = "";
        Log log = new Log();
        if (Flags.isAUTOMATIC_SAVING()) {
            log.create("SelectionToNewPopulation.txt", true);
        }

        childrenList.get(0).setGeneration(generation);
        childrenList.get(1).setGeneration(generation);

        //Adiciona os filhos na população
        this.population.add((Individual) childrenList.get(0));
        this.population.add((Individual) childrenList.get(1));

        //Ordena os indivíduos da população
        Collections.sort(this.population.getIndividuals(), new GraphBundledComparable());
        Individual worstIndividual1 = this.population.getIndividuals().get(this.population.getIndividuals().size() - 1);
        Individual worstIndividual2 = this.population.getIndividuals().get(this.population.getIndividuals().size() - 2);

        //Retira os dois piores
        this.population.remove((Individual) worstIndividual1);
        this.population.remove((Individual) worstIndividual2);

        if (Flags.isAUTOMATIC_SAVING()) {
            msgLog += "Adicionado 0:" + childrenList.get(0).getFitness() + "\n";
            msgLog += "Adicionado 1:" + childrenList.get(1).getFitness() + "\n";
            msgLog += "Removido 1:" + worstIndividual1.getFitness() + "\n";
            msgLog += "Removido 2:" + worstIndividual2.getFitness() + "\n\n";
            log.save(msgLog);
            log.close();
        }

    }

    //*************************************************************************************************************
    /**
     * Steady State
     * Responsável por alterar a população com o novos indivíduos Insere os
     * filhos na população e depois retira os pais
     *
     * @param childrenList - Lista de indivíduos filhos
     */
//*************************************************************************************************************    
    public void insertChildren(ArrayList<Individual> children, ArrayList<Individual> parents, int generations) {
        String msgLog = "";
        Log log = new Log();
        if (Flags.isAUTOMATIC_SAVING()) {
            log.create("SelectionToNewPopulation.txt", true);
        }

        //Se os pais forem distintos
        if (parents.get(0) != parents.get(1)) {
            insertChild(children.get(0), generations);
            insertChild(children.get(1), generations);
            removeParent(parents.get(0));
            removeParent(parents.get(1));

            if (Flags.isAUTOMATIC_SAVING()) {
                msgLog += "Adicionado 0:" + children.get(0).getFitness() + "\n";
                msgLog += "Adicionado 1:" + children.get(1).getFitness() + "\n";
                msgLog += "Removido 1:" + parents.get(0).getFitness() + "\n";
                msgLog += "Removido 2:" + parents.get(1).getFitness() + "\n\n";
                log.save(msgLog);
                log.close();
            }
        } else {

            //Retira o pai e insere o melhor filho
            removeParent(parents.get(0));

            if (children.get(0).getFitness() >= children.get(1).getFitness()) {
                insertChild(children.get(0), generations);

                if (Flags.isAUTOMATIC_SAVING()) {
                    msgLog += "Adicionado:" + children.get(0).getFitness() + "\n";
                    msgLog += "Removido:" + parents.get(0).getFitness() + "\n\n";
                    log.save(msgLog);
                    log.close();
                }

            } else {
                insertChild(children.get(1), generations);

                if (Flags.isAUTOMATIC_SAVING()) {
                    msgLog += "Adicionado:" + children.get(1).getFitness() + "\n";
                    msgLog += "Removido:" + parents.get(0).getFitness() + "\n\n";
                    log.save(msgLog);
                    log.close();
                }
            }
        }
    }

    //*************************************************************************************************************
    /**
     * Responsável por alterar a população com o novos indivíduos Insere os
     * filhos na população
     *
     * @param childrenList - Lista de indivíduos filhos
     */
//*************************************************************************************************************    
    private void insertChild(Individual child, int generation) {
        String msgLog = "";
        Log log = new Log();
        if (Flags.isAUTOMATIC_SAVING()) {
            log.create("SelectionToNewPopulation.txt", true);
        }

        child.setGeneration(generation);
        this.population.add((Individual) child);

        if (Flags.isAUTOMATIC_SAVING()) {
            msgLog += "Adicionado:" + child.getFitness() + "\n";
            log.save(msgLog);
            log.close();
        }
    }

    //*************************************************************************************************************
    /**
     * Responsável por alterar a população removendo os pais
     *
     * @param childrenList - Lista de indivíduos filhos
     */
//*************************************************************************************************************    
    private void removeParent(Individual parent) {
        String msgLog = "";
        Log log = new Log();
        if (Flags.isAUTOMATIC_SAVING()) {
            log.create("SelectionToNewPopulation.txt", true);
        }

        this.population.remove(parent);

        if (Flags.isAUTOMATIC_SAVING()) {
            msgLog += "Removido:" + parent.getFitness() + "\n";
            log.save(msgLog);
            log.close();
        }
    }

}
