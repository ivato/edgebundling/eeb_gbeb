package edge.bundling.methods.genetic;

import edge.bundling.graph.Bundle;
import edge.bundling.graph.Edge;
import edge.bundling.graph.GraphBundled;
import edge.bundling.graph.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Individual extends GraphBundled {

    private int generation = 1;

    public Individual(boolean direct) {
        super(direct);
    }

    public Individual() {
        super(false);
    }

    //*************************************************************************************************************
    /**
     * Calcula os parâmetros de qualidade do individuo
     *
     */
//*************************************************************************************************************                    
    public void evaluate(Population population) {
        fitness();
        population.setMaxMinValues(this);
    }

    public Individual clone() {
        Individual clone = new Individual(this.getDirected());
        ArrayList<Bundle> bundleList = new ArrayList();
        List coverList = new ArrayList();
        ArrayList<Node> nodeList = new ArrayList();
        ArrayList<Edge> edgeList = new ArrayList();

        ListIterator<Bundle> listIterator = this.bundleList.listIterator();
        while (listIterator.hasNext()) {
            Bundle bundle = (Bundle) listIterator.next().clone();
            bundleList.add(bundle);
        }

        ListIterator listIterator2 = this.coverList.listIterator();
        while (listIterator2.hasNext()) {
            coverList.add((int) listIterator2.next());
        }

        ListIterator listIterator3 = this.nodeList.listIterator();
        while (listIterator3.hasNext()) {
            nodeList.add((Node) listIterator3.next());
        }

        ListIterator listIterator4 = this.edgeList.listIterator();
        while (listIterator4.hasNext()) {
            edgeList.add((Edge) listIterator4.next());
        }

        clone.nodeList = nodeList;
        clone.edgeList = edgeList;
        clone.sizeEdge = this.sizeEdge;
        clone.sizeNode = this.sizeNode;
        clone.area = this.area;
        clone.isDirected = this.isDirected;
        clone.movedNodes = this.movedNodes;

        clone.bundleList = bundleList;
        clone.coverList = coverList;
        clone.fitness = this.fitness;
        clone.compatibility = this.compatibility;
        clone.numberOfBundles = this.getNumberOfBundles();
        clone.numberOfBundlesMoreThanOne = this.getNumberOfBundlesMoreThanOne();
        clone.numberOfBundlesEqualOne = this.getNumberOfBundlesEqualOne();
        clone.bundleIndex = this.bundleIndex;
        clone.density = this.density;
        clone.generation = this.generation;
        clone.fitnessRank = this.fitnessRank;
        clone.distance = this.distance;
        return clone;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }
}
