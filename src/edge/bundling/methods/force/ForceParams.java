package edge.bundling.methods.force;

public class ForceParams {

    private static int CYCLES = 6;
    private static int INTERACTIONS = 500;
    private static int SUBDIVISIONS = 1;
    private static double STEPSIZE = 0.4;
    private static int STIFFINESS = 5;

    public void resetToDefaults() {
        CYCLES = 6;
        INTERACTIONS = 500;
        SUBDIVISIONS = 1;
        STEPSIZE = 0.4;
        STIFFINESS = 5;
    }
    
    public static int CYCLES() {
        return CYCLES;
    }

    public static void CYCLES(int aCYCLES) {
        CYCLES = aCYCLES;
    }

    public static int INTERACTIONS() {
        return INTERACTIONS;
    }

    public static void INTERACTIONS(int aINTERACTIONS) {
        INTERACTIONS = aINTERACTIONS;
    }

    public static double STEPSIZE() {
        return STEPSIZE;
    }

    public static void STEPSIZE(double aSTEPSIZE) {
        STEPSIZE = aSTEPSIZE;
    }

    public static int STIFFINESS() {
        return STIFFINESS;
    }

    public static void STIFFINESS(int aSTIFFINESS) {
        STIFFINESS = aSTIFFINESS;
    }
    
    public static int SUBDIVISIONS() {
        return SUBDIVISIONS;
    }

    public static void SUBDIVISIONS(int aSUBDIVISIONS) {
        SUBDIVISIONS = aSUBDIVISIONS;
    }
}
