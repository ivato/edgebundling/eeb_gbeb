package edge.bundling.methods.force;

import edge.bundling.compatibility.Compatibility;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundled;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import java.util.ArrayList;
import java.util.LinkedList;

public class Force {

    private ArrayList<Edge> edges;
    private boolean bundlingActive = false;
    private Graph graph;

    public Force(Graph graph) {
        this.graph = graph;
        this.edges = graph.getEdgeList();
        this.graph.startSubNodesList();
    }

    public void run() {
        int cycles = ForceParams.CYCLES();
        int iterations = ForceParams.INTERACTIONS();
        double stepSize = ForceParams.STEPSIZE();
        int stiffness = ForceParams.STIFFINESS();

        //Controla os ciclos do processo de bundling
        for (int c = 0; c < cycles; c++) {

            //Aumenta a quantidade de seguimentos da aresta
            for (Edge e : this.edges) {
                e.increaseSubNodes();
            }

            //Controla a quantidade de iterações por ciclo
            for (int i = 0; i < iterations; i++) {

                for (Edge edgeP : this.edges) {

                    double kp = stiffness / edgeP.distance() * (edgeP.getSubNodes().size() - 1);

                    ArrayList<Node> tempSubNodes = new ArrayList<Node>();
                    tempSubNodes.add(edgeP.getStartNode());

                    for (int n = 1; n < edgeP.getSubNodes().size() - 1; n++) {

                        double electrostaticForceX = 0;
                        double electrostaticForceY = 0;

                        Node p = edgeP.getSubNodes().get(n);
                        Node pPrevious = edgeP.getSubNodes().get(n - 1);
                        Node pNext = edgeP.getSubNodes().get(n + 1);

                        //Recupera o vértice de maior grau de P
                        Node nodeStartP = edgeP.getStartNode();
                        Node nodeEndP = edgeP.getEndNode();
                        Node nodeMaxDegreeP;
                        if (nodeStartP.getNumberOfEdges() >= nodeEndP.getNumberOfEdges()) {
                            nodeMaxDegreeP = nodeStartP;
                        } else {
                            nodeMaxDegreeP = nodeEndP;
                        }

                        //Recupera a lista de arestas adjacentes ao vértice de maior grau de P
                        LinkedList<Edge> edgeList = new LinkedList<Edge>();
                        edgeList = nodeMaxDegreeP.getAdjEdgeList();

                        // Calculate electrostatic force
                        for (Edge edgeQ : edgeList) {
                            if (edgeP != edgeQ) {

                                //Caso os vértices de maior grau coincidam calcula a força entre eles
                                Node nodeStartQ = edgeQ.getStartNode();
                                Node nodeEndQ = edgeQ.getEndNode();
                                Node nodeMaxDegreeQ;
                                if (nodeStartQ.getNumberOfEdges() >= nodeEndQ.getNumberOfEdges()) {
                                    nodeMaxDegreeQ = nodeStartQ;
                                } else {
                                    nodeMaxDegreeQ = nodeEndQ;
                                }

                                if (nodeMaxDegreeP == nodeMaxDegreeQ) {

                                    Compatibility compatibility = new Compatibility();
                                    double threshold = Flags.COMPATIBILITY_THRESHOLD();
                                    double comp = edgeP.compatibility(edgeQ);

                                    if (comp >= threshold) {
                                        Node q = edgeQ.getSubNodes().get(n);
                                        double nDist = Double.POSITIVE_INFINITY;
                                        for (Node u : edgeQ.getSubNodes()) {
                                            if (p.distance(u) < nDist) {
                                                nDist = p.distance(u);
                                                q = u;
                                            }
                                        }

                                        if (p.distance(q) > 0) {
                                            electrostaticForceX += comp * ((q.getX() - p.getX()) / p.distance(q));
                                            electrostaticForceY += comp * ((q.getY() - p.getY()) / p.distance(q));
                                        }
                                    }

                                }
                            }
                        }

                        double springForceX = kp * ((pPrevious.getX() - p.getX())) + kp * ((pNext.getX() - p.getX()));
                        double springForceY = kp * ((pPrevious.getY() - p.getY())) + kp * ((pNext.getY() - p.getY()));

                        double x = p.getX() + electrostaticForceX * stepSize + springForceX * stepSize + electrostaticForceX * stepSize;
                        double y = p.getY() + electrostaticForceY * stepSize + springForceY * stepSize + electrostaticForceY * stepSize;

                        tempSubNodes.add(new Node(x, y));
                    }

                    tempSubNodes.add(edgeP.getEndNode());
                    edgeP.setTempSubNodes(tempSubNodes);
                }

                for (Edge e : this.edges) {
                    e.finalizeSubNodes();
                }
            }
            // Decrease iterations and stepSize for next cycle
            iterations = (int) Math.round(iterations * 2 / 3);
            stepSize = stepSize / 2;
        }
    }
}
