package edge.bundling.compatibility;

import edge.bundling.graph.Bundle;
import edge.bundling.graph.Constraints;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import edge.bundling.util.Geometric;
import edge.bundling.util.GraphData;
import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;

public class Compatibility {

    public double xP;
    public double yP;
    public double xPc;
    public double yPc;

    public double xQ;
    public double yQ;
    public double xQc;
    public double yQc;

    public double compatibility(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        return GraphData.getCompatibility(edge1.getId(), edge2.getId());
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade do feixe - somatória das compatibilidades das
     * arestas duas a duas
     *
     * @param bundle - feixe para o qual será calculada a compatibilidade
     * @return totalCompatibility
     */
//*************************************************************************************************************     
    public double compatibility(Bundle bundle) {
        double min = 1;
        double compatibility;
        int penaltyCount = 0;

        double threshold = Flags.COMPATIBILITY_THRESHOLD();

        Edge edge1;
        Edge edge2;

        double totalBundleCompatibility = 0;

        //Recupera as arestas do feixe
        LinkedList<Edge> listEdgeBundle = bundle.getEdges();
        int nrEdges = listEdgeBundle.size();

        if (nrEdges > 1) {

            edge1 = (Edge) listEdgeBundle.get(0);
            edge2 = (Edge) listEdgeBundle.get(1);
            min = GraphData.getCompatibility(edge1.getId(), edge2.getId());

            //Compara cada aresta com as outras arestas do feixe
            for (int j = 0; j < nrEdges; j++) {
                edge1 = (Edge) listEdgeBundle.get(j);

                //Calcula a compatibilidade com as outras arestas
                for (int i = (j + 1); i < nrEdges; i++) {
                    edge2 = (Edge) listEdgeBundle.get(i);

                    compatibility = GraphData.getCompatibility(edge1.getId(), edge2.getId());

                    if (min > compatibility) {
                        min = compatibility;
                    }

                    compatibility = scaling(compatibility);
                    totalBundleCompatibility += compatibility;
                }
            }
            
            

            //Verifica se o menor valor de compatibilidade do feixe ultrapassa o threshold
            //Caso afirmativo seta uma penalidade de 1 e estabelece totalBundleCompatibility = -1
            //Isso diferencia o caso de ter apenas uma aresta que tem totalBundleCompatibility = 0
            if (min < threshold) {
                penaltyCount = 1;
                
                if (totalBundleCompatibility > 0)
                    totalBundleCompatibility = totalBundleCompatibility * (-3);
                else
                    if (totalBundleCompatibility == 0)             
                        totalBundleCompatibility = -3;
            }

        } else {
            totalBundleCompatibility = 0;
        }

        bundle.setPenalty(penaltyCount);

        return (totalBundleCompatibility);
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade total
     *
     * @return totalCompatibility
     */
//*************************************************************************************************************    
    public double totalCompatibility(Edge edge1, Edge edge2) {
       // System.out.println("\nEdge: " + edge1.getId() + " Edge: " + edge2.getId());
        setCoordinates(edge1, edge2);
        double c = execute();

       // System.out.println("Compatibilidade: " + c);
        return c;
    }

    //*************************************************************************************************************
    /**
     * Método auxiliar que executa os calculos de compatibilidade entre duas
     * arestas
     *
     * @return totalCompatibility
     */
//*************************************************************************************************************     
    private double execute() {
        double Ca = 1;
        double Cs = 1;
        double Cp = 1;
        double Cv = 1;
        double Cd = 1;
        double Ct;

        if (Flags.isANGULAR()) {
            Ca = angular();
        }

        if (Flags.isSCALE()) {
            Cs = scale();
        }

        if (Flags.isPOSITION()) {
            Cp = position();
        }

        if (Flags.isVISIBILITY()) {
            Cv = visibility();
        }

        if (Flags.isDISTANCE()) {
            Cd = distance();
        }

        Ct = Ca * Cs * Cp * Cv * Cd;

        //System.out.println("Compatibilidade: " + Ct + "\n");
        return Ct;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade angular entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return angle
     */
//*************************************************************************************************************     
    private double angular() {
        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        double angularCompatibility = Geometric.angleBetween2Lines(p1, p2, p3, p4);

        if (angularCompatibility != -1) {
            angularCompatibility = Math.cos(angularCompatibility);
            BigDecimal value = new BigDecimal(angularCompatibility).setScale(3, RoundingMode.HALF_DOWN);
           // System.out.println("Angular:  " + value.doubleValue());
            return value.doubleValue();
        } else {
            angularCompatibility = 1;
        }
        //System.out.println("Angular:  " + angularCompatibility);

        return angularCompatibility;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade angular entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return angle
     */
//*************************************************************************************************************     
    public double angular(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Ca = angular();
        return Ca;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de escala entre duas arestas
     *
     * @return escala
     */
//*************************************************************************************************************     
    private double scale() {
        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        double le1 = Geometric.euclideanDistance(p1, p2);
        double le2 = Geometric.euclideanDistance(p3, p4);

        double lavg = (le1 + le2) / 2.0;
        double scaleCompatibility = 2.0 / ((lavg / Math.min(le1, le2)) + (Math.max(le1, le2) / lavg));
        BigDecimal value = new BigDecimal(scaleCompatibility).setScale(3, RoundingMode.HALF_DOWN);

        //System.out.println("Escala: " + value.doubleValue());
        return value.doubleValue();
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de escala entre duas arestas
     *
     * @return escala
     */
//*************************************************************************************************************     
    public double scale(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Cs = scale();
        return Cs;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de posição entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return posição
     */
//*************************************************************************************************************     
    private double position() {
        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        double lavg = (Geometric.euclideanDistance(p1, p2) + Geometric.euclideanDistance(p3, p4)) / 2.0;
        Point.Double midP = new Point.Double((p2.x + p1.x) / 2.0, (p2.y + p1.y) / 2.0);
        Point.Double midQ = new Point.Double((p4.x + p3.x) / 2.0, (p4.y + p3.y) / 2.0);
        double positionCompatibility = lavg / (lavg + Geometric.euclideanDistance(midP, midQ));
        BigDecimal value = new BigDecimal(positionCompatibility).setScale(3, RoundingMode.HALF_DOWN);

        //System.out.println("Posição: " + value.doubleValue());
        return value.doubleValue();
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de posição entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return posição
     */
//*************************************************************************************************************         
    public double position(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Cp = position();
        return Cp;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de visibilidade entre duas arestas retorna um
     * valor entre 0 e 1
     *
     * @return visibilidade
     */
//*************************************************************************************************************     
    /* public double visibility2() {

        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        Point.Double ds = new Point.Double(p3.getX() + Geometric.dy(p1, p2), p3.getY() - Geometric.dx(p1, p2));
        Point.Double de = new Point.Double(p4.getX() + Geometric.dy(p1, p2), p4.getY() - Geometric.dx(p1, p2));
        Node i0 = Geometric.lineIntersection(p1, p2, p3, ds);
        Node i1 = Geometric.lineIntersection(p1, p2, p4, de);
        Node im = new Edge(i0, i1).midPoint();
        if (i0 == null || im == null || i1 == null) {
            return 0;
        }
        double VPQ = Math.max(0, 1 - (2 * Geometric.midPoint(p1, p2).distance(im)) / i0.distance(i1));

        ds = new Point.Double(p1.getX() + Geometric.dy(p3, p4), p1.getY() - Geometric.dx(p3, p4));
        de = new Point.Double(p2.getX() + Geometric.dy(p3, p4), p2.getY() - Geometric.dx(p3, p4));
        i0 = Geometric.lineIntersection(p3, p4, p1, ds);
        i1 = Geometric.lineIntersection(p3, p4, p2, de);
        im = new Edge(i0, i1).midPoint();
        if (i0 == null || im == null || i1 == null) {
            return 0;
        }
        double VQP = Math.max(0, 1 - (2 * Geometric.midPoint(p3, p4).distance(im)) / i0.distance(i1));

        double visibilityCompatibility = Math.min(VPQ, VQP);
        BigDecimal value = new BigDecimal(visibilityCompatibility).setScale(4, RoundingMode.HALF_DOWN);

        System.out.println("Visibilidade2: " + value.doubleValue());
        return value.doubleValue();
    }*/
    
    
    public double visibility() {

        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        Point.Double I0 = Geometric.project(p1, p3, p4);
        Point.Double I1 = Geometric.project(p2, p3, p4);

        Point.Double midI = Geometric.center(I0, I1);
        Point.Double midP = Geometric.center(p3, p4);

        double visibilityCompatibility = Math.max(0.0, 1.0 - ((2.0 * Geometric.euclideanDistance(midP, midI)) / Geometric.euclideanDistance(I0, I1)));

        BigDecimal value = new BigDecimal(visibilityCompatibility).setScale(4, RoundingMode.HALF_DOWN);
        //System.out.println("Visibilidade3: " + value.doubleValue());

        return value.doubleValue();
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de visibilidade entre duas arestas retorna um
     * valor entre 0 e 1
     *
     * @return visibilidade
     */
//*************************************************************************************************************     
    public double visibility(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Cv = visibility();
        return Cv;
    }

    public double distance(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Cd;
        Cd = distance();
        return Cd;
    }

    public double distance() {

        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);
        double distanceCompatibility;

        //Calculando distância entre pontos médios
        Node nodeStart1 = new Node(xP, yP);
        Node nodeEnd1 = new Node(xPc, yPc);
        Edge edge1 = new Edge(nodeStart1, nodeEnd1);

        Node nodeStart2 = new Node(xQ, yQ);
        Node nodeEnd2 = new Node(xQc, yQc);
        Edge edge2 = new Edge(nodeStart2, nodeEnd2);

        double k = 60.0;
        Node np = new Node();
        np = (Node) edge1.midPoint();

        Node nq = new Node();
        nq = (Node) edge2.midPoint();

        Point.Double p = new Point.Double(np.getX(), np.getY());
        Point.Double q = new Point.Double(nq.getX(), nq.getY());

        double dmid = Geometric.euclideanDistance(p, q);
        distanceCompatibility = 1 - (dmid / Flags.XDIST());

        //Calculando a distância máxima e mínima
        /*double dist1, dist2;
        if (Geometric.euclideanDistance(p1, p3) < Geometric.euclideanDistance(p1, p4)) {
                dist1 = Geometric.euclideanDistance(p1, p3);
                dist2 = Geometric.euclideanDistance(p2, p4);
        } else {
                dist1 = Geometric.euclideanDistance(p1, p4);
                dist2 = Geometric.euclideanDistance(p2, p3);
        }
        double dmax = Math.max(dist1, dist2);
        double dmin = Math.min(dist1, dist2);
        distanceCompatibility = dmin;*/
 /*Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        Node nodeStart1 = new Node(xP, yP);
        Node nodeEnd1 = new Node(xPc, yPc);
        Edge edge1 = new Edge(nodeStart1, nodeEnd1);
        Node nodeStart2 = new Node(xQ, yQ);
        Node nodeEnd2 = new Node(xQc, yQc);
        Edge edge2 = new Edge(nodeStart2, nodeEnd2);

        
        if (!edge1.isAdjacent(edge2)) {
            Node np = new Node();
            np = (Node) Geometric.midPoint(p1, p2);
            Node nq = new Node();
            nq = (Node) Geometric.midPoint(p3, p4);

            Point.Double p = new Point.Double(np.getX(), np.getY());
            Point.Double q = new Point.Double(nq.getX(), nq.getY());
            double dmax = Geometric.euclideanDistance(p, q);
            distanceCompatibility = 1 - Math.sqrt(1 / (1 + (dmax)));
        } else {
            double dist1, dist2;
            if (Geometric.euclideanDistance(p1, p3) < Geometric.euclideanDistance(p1, p4)) {
                dist1 = Geometric.euclideanDistance(p1, p3);
                dist2 = Geometric.euclideanDistance(p2, p4);
            } else {
                dist1 = Geometric.euclideanDistance(p1, p4);
                dist2 = Geometric.euclideanDistance(p2, p3);
            }
            double dmax = Math.max(dist1, dist2);
            distanceCompatibility = 1 - Math.sqrt(1 / (1 + (dmax)));
        }*/
        BigDecimal value = new BigDecimal(distanceCompatibility).setScale(4, RoundingMode.HALF_DOWN);
      //  System.out.println("Distância: " + value.doubleValue());

        return value.doubleValue();
    }

    //*************************************************************************************************************
    /**
     * Calcula o escalonamento da medida de compatibilidade
     *
     * @return compatibility
     */
//*************************************************************************************************************     
    private double scaling(double compatibility) {
        double threshold = Flags.COMPATIBILITY_THRESHOLD();
        return (1 + ((compatibility - threshold) / threshold));
    }

    //*************************************************************************************************************
    /**
     * Calcula o threshold total baseado em todas as medidas de compatibilidade
     *
     * @return threshold
     */
//*************************************************************************************************************     
    public double threshold() {
        double threshold = 1;

        if (Flags.isANGULAR()) {
            threshold *= Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD();
        }
        if (Flags.isSCALE()) {
            threshold *= Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD();
        }
        if (Flags.isPOSITION()) {
            threshold *= Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD();
        }
        if (Flags.isVISIBILITY()) {
            threshold *= Flags.COMPATIBILITY_MAX_VISIBILITY_THRESHOLD();
        }
        if (Flags.isDISTANCE()) {
            threshold *= Flags.COMPATIBILITY_MAX_DISTANCE_THRESHOLD();
        }

        Flags.COMPATIBILITY_THRESHOLD(threshold);

        //System.out.println("New Threshold: " + threshold);
        return threshold;
    }

    //*************************************************************************************************************
    /**
     * Seta as coordenadas dos pontos da arestas
     *
     * @param edge1 - aresta 1
     * @param edge1 - aresta 2
     */
//*************************************************************************************************************        
    private void setCoordinates(Edge edge1, Edge edge2) {

        double x1, y1, x2, y2, x3, y3, x4, y4;

        x1 = edge1.getStartNode().getX();
        y1 = edge1.getStartNode().getY();
        x2 = edge1.getEndNode().getX();
        y2 = edge1.getEndNode().getY();

        x3 = edge2.getStartNode().getX();
        y3 = edge2.getStartNode().getY();
        x4 = edge2.getEndNode().getX();
        y4 = edge2.getEndNode().getY();

        this.xP = x1;
        this.yP = y1;
        this.xPc = x2;
        this.yPc = y2;

        this.xQ = x3;
        this.yQ = y3;
        this.xQc = x4;
        this.yQc = y4;
    }
}
