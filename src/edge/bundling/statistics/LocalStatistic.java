
package edge.bundling.statistics;

public class LocalStatistic {
    
    private double sumLocalFitness = 0.0;
    private double quantityLocalIndividuals = 0.0;
    private Double bestLocalFitness = null;
    private Double worstLocalFitness = null;


    public double getSumLocalFitness() {
        return sumLocalFitness;
    }

    public double getQuantityLocalIndividuals() {
        return quantityLocalIndividuals;
    }

    public double getBestLocalFitness() {
        return bestLocalFitness;
    }

    public double getWorstLocalFitness() {
        return worstLocalFitness;
    }

    public void setSumLocalFitness(double sumLocalFitness) {
        this.sumLocalFitness = sumLocalFitness;
    }

    public void setQuantityLocalIndividuals(double quantityLocalIndividuals) {
        this.quantityLocalIndividuals = quantityLocalIndividuals;
    }

    public void setBestLocalFitness(Double bestLocalFitness) {
        this.bestLocalFitness = bestLocalFitness;
    }

    public void setWorstLocalFitness(Double worstLocalFitness) {
        this.worstLocalFitness = worstLocalFitness;
    }
}
