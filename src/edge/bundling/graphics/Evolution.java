package edge.bundling.graphics;

import edge.bundling.methods.genetic.Individual;
import edge.bundling.methods.genetic.Population;
import java.awt.Color;
import java.util.ListIterator;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Evolution extends JFrame {

    Population population1 = new Population();
    Population population2 = new Population();

    public Evolution(String title, Population population1 , Population population2) {

        super(title);
        this.population1 = population1;
        this.population2 = population2;

        XYDataset dataset = createDataset(population1, population2);
        JFreeChart lineChart = createChart(dataset);
        
        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1120, 800));
        setContentPane(chartPanel);
      
    }

    private JFreeChart createChart(XYDataset dataset) {

        final JFreeChart chart = ChartFactory.createXYLineChart(
                "Evolução por Geração",
                "Geração",
                "Fitness",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        
        chart.setBackgroundPaint(Color.white);

        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
                
        return chart;
    }
    
    private XYDataset createDataset(Population population1, Population population2) {
        
        int generation = 1;        
        final XYSeries series1 = new XYSeries("População 1");
        ListIterator<Individual> listIterator = population1.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            series1.add(generation, individual.getFitness());
            generation++;
        }

        generation = 1;
        final XYSeries series2 = new XYSeries("População 2");
        ListIterator<Individual> listIterator2 = population2.getIndividuals().listIterator();
        while (listIterator2.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator2.next();
            series2.add(generation, individual.getFitness());
            generation++;
        }
        
        
        
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        
        return dataset;
    }
}


/*package edge.bundling.graphics;

import edge.bundling.methods.genetic.Individual;
import edge.bundling.methods.genetic.Population;
import java.awt.Color;
import java.util.ListIterator;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Evolution extends JFrame {

    Population population1 = new Population();
    Population population2 = new Population();
    Population population3 = new Population();

    public Evolution(String title, Population population1 , Population population2 , Population population3) {

        super(title);
        this.population1 = population1;
        this.population2 = population2;
        this.population3 = population3;

        XYDataset dataset = createDataset(population1, population2, population3);
        JFreeChart lineChart = createChart(dataset);
        
        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1120, 800));
        setContentPane(chartPanel);
      
    }

    private JFreeChart createChart(XYDataset dataset) {

        final JFreeChart chart = ChartFactory.createXYLineChart(
                "Evolução por Geração",
                "Geração",
                "Fitness",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        
        chart.setBackgroundPaint(Color.white);

        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
                
        return chart;
    }
    
    private XYDataset createDataset(Population population1, Population population2, Population population3) {
        
        int generation = 1;        
        final XYSeries series1 = new XYSeries("População 1");
        ListIterator<Individual> listIterator = population1.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            series1.add(generation, individual.getFitness());
            generation++;
        }

        generation = 1;
        final XYSeries series2 = new XYSeries("População 2");
        ListIterator<Individual> listIterator2 = population2.getIndividuals().listIterator();
        while (listIterator2.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator2.next();
            series2.add(generation, individual.getFitness());
            generation++;
        }
        
        
        generation = 1;
        final XYSeries series3 = new XYSeries("População 3");
        ListIterator<Individual> listIterator3 = population3.getIndividuals().listIterator();
        while (listIterator3.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator3.next();
            series3.add(generation, individual.getFitness());
            generation++;
        }
        
        
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        dataset.addSeries(series3);
        
        return dataset;
    }
}*/
