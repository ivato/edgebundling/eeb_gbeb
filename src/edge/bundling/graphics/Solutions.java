package edge.bundling.graphics;

import edge.bundling.methods.genetic.Individual;
import edge.bundling.methods.genetic.Population;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.io.FileNotFoundException;
import java.util.ListIterator;

import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class Solutions extends JFrame {

    Population initialPopulation1 = new Population();
    Population initialPopulation2 = new Population();

    public Solutions(String title, Population initialPopulation1, Population initialPopulation2, Individual bestIndividual) throws FileNotFoundException {

        super(title);
        this.initialPopulation1 = initialPopulation1;
        this.initialPopulation2 = initialPopulation2;

        XYDataset dataset = createDataset(initialPopulation1, initialPopulation2, bestIndividual);

        JFreeChart chart = ChartFactory.createScatterPlot("Qualidade das Soluções",
                "Número de Feixes",
                "Compatibilidade",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        Shape shape = ShapeUtilities.createDiamond(3);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(new Color(255, 228, 196));

        plot.setDomainPannable(true);
        plot.setDomainZeroBaselineVisible(true);
        plot.setDomainGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainGridlinePaint(Color.blue);
        plot.setDomainMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainMinorGridlinesVisible(true);

        plot.setRangePannable(true);
        plot.setRangeZeroBaselineVisible(true);
        plot.setRangeGridlinePaint(Color.blue);
        plot.setRangeGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlinesVisible(true);

        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesOutlinePaint(0, Color.black);
        renderer.setUseOutlinePaint(true);
        renderer.setSeriesShape(0, shape);

        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setTickMarkInsideLength(2.0f);
        domainAxis.setTickMarkOutsideLength(2.0f);
        domainAxis.setMinorTickCount(2);
        domainAxis.setMinorTickMarksVisible(true);

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setTickMarkInsideLength(2.0f);
        rangeAxis.setTickMarkOutsideLength(2.0f);
        rangeAxis.setMinorTickCount(2);
        rangeAxis.setMinorTickMarksVisible(true);

        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);

    }

    private XYDataset createDataset(Population initialPopulation1, Population initialPopulation2, Individual bestIndividual) {

        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series1 = new XYSeries("População Inicial 1");
        XYSeries series2 = new XYSeries("População Inicial 2");
        XYSeries series4 = new XYSeries("Melhor Solução");

        int numberOfBundles;
        double compatibility;

        ListIterator<Individual> listIterator = initialPopulation1.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series1.add(numberOfBundles, compatibility);
        }

        listIterator = initialPopulation2.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series2.add(numberOfBundles, compatibility);
        }
       
        numberOfBundles = bestIndividual.getNumberOfBundles();
        compatibility = bestIndividual.getCompatibility();
        series4.add(numberOfBundles, compatibility);

        dataset.addSeries(series4);
        dataset.addSeries(series1);
        dataset.addSeries(series2);

        return dataset;
    }
}




/*package edge.bundling.graphics;

import edge.bundling.methods.genetic.Individual;
import edge.bundling.methods.genetic.Population;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.io.FileNotFoundException;
import java.util.ListIterator;

import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class Solutions extends JFrame {

    Population initialPopulation1 = new Population();
    Population initialPopulation2 = new Population();
    Population initialPopulation3 = new Population();

    public Solutions(String title, Population initialPopulation1, Population initialPopulation2, Population initialPopulation3, Individual bestIndividual) throws FileNotFoundException {

        super(title);
        this.initialPopulation1 = initialPopulation1;
        this.initialPopulation2 = initialPopulation2;
        this.initialPopulation3 = initialPopulation3;

        XYDataset dataset = createDataset(initialPopulation1, initialPopulation2, initialPopulation3, bestIndividual);

        JFreeChart chart = ChartFactory.createScatterPlot("Qualidade das Soluções",
                "Número de Feixes",
                "Compatibilidade",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        Shape shape = ShapeUtilities.createDiamond(3);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(new Color(255, 228, 196));

        plot.setDomainPannable(true);
        plot.setDomainZeroBaselineVisible(true);
        plot.setDomainGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainGridlinePaint(Color.blue);
        plot.setDomainMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainMinorGridlinesVisible(true);

        plot.setRangePannable(true);
        plot.setRangeZeroBaselineVisible(true);
        plot.setRangeGridlinePaint(Color.blue);
        plot.setRangeGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlinesVisible(true);

        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesOutlinePaint(0, Color.black);
        renderer.setUseOutlinePaint(true);
        renderer.setSeriesShape(0, shape);

        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setTickMarkInsideLength(2.0f);
        domainAxis.setTickMarkOutsideLength(2.0f);
        domainAxis.setMinorTickCount(2);
        domainAxis.setMinorTickMarksVisible(true);

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setTickMarkInsideLength(2.0f);
        rangeAxis.setTickMarkOutsideLength(2.0f);
        rangeAxis.setMinorTickCount(2);
        rangeAxis.setMinorTickMarksVisible(true);

        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);

    }

    private XYDataset createDataset(Population initialPopulation1, Population initialPopulation2, Population initialPopulation3, Individual bestIndividual) {

        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series1 = new XYSeries("População Inicial 1");
        XYSeries series2 = new XYSeries("População Inicial 2");
        XYSeries series3 = new XYSeries("População Inicial 3");
        XYSeries series4 = new XYSeries("Melhor Solução");

        int numberOfBundles;
        double compatibility;

        ListIterator<Individual> listIterator = initialPopulation1.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series1.add(numberOfBundles, compatibility);
        }

        listIterator = initialPopulation2.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series2.add(numberOfBundles, compatibility);
        }
        
        listIterator = initialPopulation3.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series3.add(numberOfBundles, compatibility);
        }

        numberOfBundles = bestIndividual.getNumberOfBundles();
        compatibility = bestIndividual.getCompatibility();
        series4.add(numberOfBundles, compatibility);

        dataset.addSeries(series4);
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        dataset.addSeries(series3);

        return dataset;
    }
}*/
