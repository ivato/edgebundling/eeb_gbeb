package edge.bundling;

import edge.bundling.compatibility.Compatibility;
import edge.bundling.util.Flags;
import javax.swing.JFrame;

public class LaunchApplication {
    public static void main(String[] args){
        MainWindow frm = new MainWindow();
        frm.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frm.setVisible(true);
        
        Compatibility compatibility = new Compatibility();
        Flags.COMPATIBILITY_THRESHOLD(compatibility.threshold());
    }
}
