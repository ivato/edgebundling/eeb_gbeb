package edge.bundling.graph;

import java.util.Comparator;

public class GraphBundledComparable implements Comparator<GraphBundled> {

   public int compareWithConstraint(GraphBundled o1, GraphBundled o2) {
       
        if (o1.getFitness() > o2.getFitness()) return -1;
        
        if (o1.getFitness() < o2.getFitness()) return 1;
        
        if (o1.getFitness() == o2.getFitness()){
            if (o1.getCompatibility() > o2.getCompatibility()){
                return -1;
            }          
            if (o1.getCompatibility() < o2.getCompatibility()){
                return 1;
            }            
            if (o1.getCompatibility() == o2.getCompatibility()){
                return 0;                  
            }    
        }
        return -1;
    }
    
     @Override
    public int compare(GraphBundled o1, GraphBundled o2) {
        
       return (o1.getFitness() > o2.getFitness() ? -1 : (o1.getFitness() == o2.getFitness() ? 0 : 1));
    }  
    
}
