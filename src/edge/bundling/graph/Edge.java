package edge.bundling.graph;

import edge.bundling.compatibility.Compatibility;
import edge.bundling.util.Flags;
import edge.bundling.util.Geometric;
import edge.bundling.util.GraphData;
import java.awt.Point;
import java.util.ArrayList;

public class Edge {

    private int id;
    private String data;
    private Node startNode;
    private Node endNode;
    private double weight;
    

    private ArrayList<Node> subNodes;
    private ArrayList<Node> tempSubNodes;

    public Edge() {
        this.id = 0;
        this.data = "";
        this.startNode = null;
        this.endNode = null;
        this.weight = 0.0;
    }

    public Edge(Node startNode, Node endNode) {
        if (startNode.getX() <= endNode.getX()) {
            this.startNode = startNode;
            this.endNode = endNode;
        } else {
            this.endNode = startNode;
            this.startNode = endNode;
        }
    }

    public Edge(int id, String data, Node v, Node w, double weight) {
        this.id = id;
        this.data = data;
        this.weight = weight;
        if (v.getX() <= w.getX()) {
            this.startNode = v;
            this.endNode = w;
        } else {
            this.endNode = v;
            this.startNode = w;
        }
    }

    
    
    public Edge(int id, String data, double weight) {
        this.id = id;
        this.data = data;
        this.weight = weight;
    }
    
    //***********************************************************************************************************
    /**
     * Calcula a distância no eixo x
     *
     */
//*************************************************************************************************************                  
    public double dx() {
        return getEndNode().getX() - getStartNode().getX();
    }

    //***********************************************************************************************************
    /**
     * Calcula a distância no eixo y
     *
     */
//*************************************************************************************************************                  
    public double dy() {
        return getEndNode().getY() - getStartNode().getY();
    }

    //***********************************************************************************************************
    /**
     * Calcula a distância euclidiana entre os extremos da aresta
     *
     */
//*************************************************************************************************************                  
    public double distance() {
        return Math.sqrt(dx() * dx() + dy() * dy());
    }

    //***********************************************************************************************************
    /**
     * Calcula o ponto médio da aresta
     *
     */
//*************************************************************************************************************                  
    public Node midPoint() {
        double x = (getStartNode().getX() + getEndNode().getX()) / 2;
        double y = (getStartNode().getY() + getEndNode().getY()) / 2;
        Node node = new Node();
        node.setX(x);
        node.setY(y);
        return node;
    }

    
            
    public double distanceBetweenEdges(Edge edge){
        /*Node midPointEdge1 = new Node();
        Node midPointEdge2 = new Node();
        
        midPointEdge1 = this.midPoint();
        midPointEdge2 = edge.midPoint();
        
        Point.Double p = new Point.Double(midPointEdge1.getX(), midPointEdge1.getY());
        Point.Double q = new Point.Double(midPointEdge2.getX(), midPointEdge2.getY());
                
        double distance = Geometric.euclideanDistance(p,q);*/
        
        double distance = GraphData.getCompatibilityDistance(this.getId(),edge.getId());              
        return distance;       
    }
    
    
    //***********************************************************************************************************
    /**
     * Lista a coordenada X dos subnodes
     *
     */
//*************************************************************************************************************                  
    public int[] subNodesXInt() {
        int[] subX = new int[subNodes.size()];
        for (int i = 0; i < subNodes.size(); i++) {
            subX[i] = subNodes.get(i).getXInt();
        }
        return subX;
    }

    //***********************************************************************************************************
    /**
     * Lista a coordenada Y dos subnodes
     *
     */
//*************************************************************************************************************                  
    public int[] subNodesYInt() {
        int[] subY = new int[subNodes.size()];
        for (int i = 0; i < subNodes.size(); i++) {
            subY[i] = subNodes.get(i).getYInt();
        }
        return subY;
    }

    //***********************************************************************************************************
    /**
     * Seta a lista de subnodes como nula
     *
     */
//*************************************************************************************************************                  
    public void resetSubNodes() {
        subNodes = null;
        tempSubNodes = null;
    }

    //***********************************************************************************************************
    /**
     * Finaliza a lista de subnodes
     *
     */
//*************************************************************************************************************                      
    public void finalizeSubNodes() {
        subNodes = tempSubNodes;
        tempSubNodes = null;
    }

    //***********************************************************************************************************
    /**
     * Instância a lista de subnodes
     *
     */
//*************************************************************************************************************                  
    public void startSubNodesList() {
        subNodes = new ArrayList<Node>();
        tempSubNodes = new ArrayList<Node>();
    }

    //***********************************************************************************************************
    /**
     * Aumenta a quantidade de subnodes presentes da aresta
     *
     */
//*************************************************************************************************************                  
    public void increaseSubNodes() {
        if (subNodes == null) {
            subNodes = new ArrayList<Node>();
            subNodes.add(startNode);
            subNodes.add(midPoint());
            subNodes.add(endNode);
        } else {
            ArrayList<Node> tempList = new ArrayList<Node>();
            for (int i = 0; i < subNodes.size() - 1; i++) {
                tempList.add(subNodes.get(i));
                tempList.add(new Node((subNodes.get(i).getX() + subNodes.get(i + 1).getX()) / 2, (subNodes.get(i).getY() + subNodes.get(i + 1).getY()) / 2));
            }
            tempList.add(endNode);
            subNodes = tempList;
        }
    }

    //***********************************************************************************************************
    /**
     * Verifica se a compatiblidade entre duas arestas
     *
     * @param edge - aresta a ser comparada
     * @return boolean
     */
//*************************************************************************************************************                  
    public double compatibility(Edge other) {
        Compatibility c = new Compatibility();
        return c.compatibility(this, other);
    }

    //***********************************************************************************************************
    /**
     * Verifica se uma aresta é adjacente a esta aresta
     *
     * @param edge - aresta a ser comparada
     * @return boolean
     */
//*************************************************************************************************************                  
    public boolean isAdjacent(Edge edge) {
        
        return GraphData.isAdjacent(this.getId(), edge.getId());
        
        /*System.out.print("[" + this.getId() + ", " + edge.getId() + "]");
        
        if (GraphData.isAdjacent(this.getId(), edge.getId())){
            System.out.print("True1 ");
        }else{
            System.out.print("False1 ");
        }
        
        if (this.getStartNode() == edge.getStartNode()) {
            System.out.println("True2");
            return true;
        }
        if (this.getStartNode() == edge.getEndNode()) {
            System.out.println("True2");
            return true;
        }
        if (this.getEndNode() == edge.getStartNode()) {
            System.out.println("True2");
            return true;
        }
        if (this.getEndNode() == edge.getEndNode()) {
            System.out.println("True2");
            return true;
        }
        System.out.println("False2");
        return false;*/
    }


    //***********************************************************************************************************
    /**
     * Verifica qual o vértice comum entre esta aresta a passada como parâmetro
     *
     * @param edge - aresta a ser comparada
     * @return node
     */
//*************************************************************************************************************                  
    public Node commonNode(Edge edge) {
        
        Node centerNode2 = null;
        if (isAdjacent(edge)) {
             centerNode2 = new Node();
             int index = GraphData.getCommomNode(this.getId(), edge.getId());
             if (index != -1){
                 centerNode2 = Flags.GRAPH().nodeList.get(index);
             }
        }
        return centerNode2;
        
        
 /*       Node centerNode1 = null;

        if (isAdjacent(edge)) {
            centerNode1 = new Node();

            Node same = new Node();
            Node same1 = new Node();
            Node same2 = new Node();

            same1 = this.getStartNode();
            same2 = edge.getStartNode();
            if (same1 == same2) {
                same = this.getStartNode();
            }

            same1 = this.getStartNode();
            same2 = edge.getEndNode();
            if (same1 == same2) {
                same = this.getStartNode();
            }

            same1 = this.getEndNode();
            same2 = edge.getStartNode();
            if (same1 == same2) {
                same = this.getEndNode();
            }

            same1 = this.getEndNode();
            same2 = edge.getEndNode();
            if (same1 == same2) {
                same = this.getEndNode();
            }

            centerNode1 = same;
        }
        
       
        return centerNode1;*/
    }

    //*************************************************************************************************************
    /**
     * Gera um clone da aresta
     *
     * @return Edge
     */
//*************************************************************************************************************               
    public Edge clone() {

        Edge edge = new Edge();
        edge.id = this.id;
        edge.data = this.data;
        edge.startNode = this.startNode;
        edge.endNode = this.endNode;
        edge.weight = this.weight;
        return edge;
    }

    public void clone(Edge edge) {
        this.id = edge.getId();
        this.data = edge.getData();
        this.startNode = edge.getStartNode();
        this.endNode = edge.getEndNode();
        this.weight = edge.getWeight();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setStartNode(Node startNode) {
        this.startNode = startNode;
    }
    
    public Node getStartNode() {
        return startNode;
    }

    public void setEndNode(Node endNode) {
        this.endNode = endNode;
    }
    
    public Node getEndNode() {
        return endNode;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
       this.weight = weight;
    }
        
    public ArrayList<Node> getSubNodes() {
        return subNodes;
    }

    public void setTempSubNodes(ArrayList<Node> subNodes) {
        tempSubNodes = subNodes;
    }
}
