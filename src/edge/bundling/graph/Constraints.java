package edge.bundling.graph;

import edge.bundling.util.Flags;
import edge.bundling.util.GraphData;
import java.util.ArrayList;
import java.util.ListIterator;

public class Constraints {

    public static boolean satisfyAdjacencyConstraint(Edge e, Bundle bundle) {
        try {
            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!edge.isAdjacent(e)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    //***********************************************************************************************************
    /**
     * Compara uma aresta com as arestas de um feixe devolver verdadeiro se ela
     * é compatível com todas do feixe
     *
     * @param e - aresta a ser comparada
     * @param bundle - feixe de arestas
     * @return boolean
     */
//*************************************************************************************************************                  
    public static boolean satisfyConstraint(Edge e, Bundle bundle) {

        try {

            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!Constraints.satisfyConstraint(e, edge)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    //***********************************************************************************************************
    /**
     * Verifica se duas arestas respeitam os thresholds determinados
     *
     * @param e1 - aresta 1 a ser comparada
     * @param e2 - aresta 2 a ser comparada
     * @return boolean
     */
//*************************************************************************************************************   
    public static boolean satisfyConstraint(Edge e1, Edge e2) {

        if (!angularConstraint(e1, e2)){return false;}
        if (!positionConstraint(e1, e2)){return false;}
        if (!scaleConstraint(e1, e2)){return false;}
        if (!visibilityConstraint(e1, e2)){return false;}
        if (!distanceConstraint(e1, e2)){return false;}

        return true;
    }

    public static boolean satisfyConstraintAngular(Edge e, Bundle bundle) {
        try {

            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!Constraints.angularConstraint(e, edge)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    public static boolean angularConstraint(Edge e1, Edge e2) {
        double comp;
        boolean testCompatibility = true;
        if (Flags.isANGULAR()) {
            comp = GraphData.getCompatibilityAngular(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD()) {
                testCompatibility = false;
            }
        }
        return testCompatibility;
    }

    public static boolean satisfyConstraintScale(Edge e, Bundle bundle) {
        try {

            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!Constraints.scaleConstraint(e, edge)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    public static boolean scaleConstraint(Edge e1, Edge e2) {
        double comp;
        boolean testCompatibility = true;
        if (Flags.isSCALE()) {
            comp = GraphData.getCompatibilityScale(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD()) {
                testCompatibility = false;
            }
        }
        return testCompatibility;
    }

    public static boolean satisfyConstraintPosition(Edge e, Bundle bundle) {
        try {

            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!Constraints.positionConstraint(e, edge)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    public static boolean positionConstraint(Edge e1, Edge e2) {
        double comp;
        boolean testCompatibility = true;
        if (Flags.isPOSITION()) {
            comp = GraphData.getCompatibilityPosition(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD()) {
                testCompatibility = false;
            }
        }
        return testCompatibility;
    }

    public static boolean satisfyConstraintVisibility(Edge e, Bundle bundle) {
        try {

            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!Constraints.visibilityConstraint(e, edge)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    public static boolean visibilityConstraint(Edge e1, Edge e2) {
        double comp;
        boolean testCompatibility = true;
        if (Flags.isVISIBILITY()) {
            comp = GraphData.getCompatibilityVisibility(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_VISIBILITY_THRESHOLD()) {
                testCompatibility = false;
            }
        }
        return testCompatibility;
    }

    public static boolean satisfyConstraintDistance(Edge e, Bundle bundle) {
        try {

            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!Constraints.distanceConstraint(e, edge)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    public static boolean distanceConstraint(Edge e1, Edge e2) {
        double comp;
        boolean testCompatibility = true;
        if (Flags.isDISTANCE()) {
            comp = GraphData.getCompatibilityDistance(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_DISTANCE_THRESHOLD()) {
                testCompatibility = false;
            }
        }
        return testCompatibility;
    }

}
